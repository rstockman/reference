# add and uncomment the following "if" block to your ~/.bashrc file
#if [ -f ~/.bash_prompt ]; then
#	. ~/.bash_prompt
#fi



# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

ps1space=" "

# date Day Month Date) bold yellow
ps1date="\[$(tput bold)\]\[\033[38;5;11m\]\d\[$(tput sgr0)\]"

# time-short (HH:MM) bold yellow
ps1time="\[$(tput bold)\]\[\033[38;5;11m\]\A\[$(tput sgr0)\]"

# username bold red
ps1username="\[$(tput bold)\]\[\033[38;5;9m\]\u\[$(tput sgr0)\]"

# at sign bold white
ps1at="\[$(tput bold)\]@\[$(tput sgr0)\]"

# hostname (short) bold light blue
ps1host="\[$(tput bold)\]\[\033[38;5;14m\]\h\[$(tput sgr0)\]"

# colon bold white
ps1colon="\[$(tput bold)\]:\[$(tput sgr0)\]"

# directory bold green
ps1directory="\[$(tput bold)\]\[\033[38;5;2m\]\w\[$(tput sgr0)\]"

ps1newline="\n"

# brackets bold white
ps1openbracket="\[$(tput bold)\][\[$(tput sgr0)\]"
ps1closebracket="\[$(tput bold)\]]\[$(tput sgr0)\]"

# exit status bold red status
ps1exitstatus="\[$(tput bold)\]\[\033[38;5;9m\]\$?\[$(tput sgr0)\]"

# prompt bold white
ps1prompt="\[$(tput bold)\]\\$\[$(tput sgr0)\]"

# git red bold
ps1git="\[$(tput bold)\]\[\033[38;5;9m\]\`parse_git_branch\`\[$(tput sgr0)\]"


# build prompt
PS1=""

# start with empty line
PS1+="$ps1newline"

# date and time
PS1+="$ps1date$ps1space$ps1time$ps1space"

# username@host:directory
PS1+="$ps1username$ps1at$ps1host$ps1colon$ps1directory$ps1space"

# git branch and status
PS1+="$ps1git"

# move the prompt to next line so long directories don't cause wrapping
PS1+="$ps1newline"

# exit status in brackets
#PS1+="$ps1openbracket$ps1exitstatus$ps1closebracket$ps1space"

# prompt charcter $ for normal user # for root user
PS1+="$ps1prompt$ps1space"

export PS1;
