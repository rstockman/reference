package rws.reference.webmvc.restapi.dao;

import rws.reference.webmvc.restapi.model.Person;

public interface PersonDAO extends CrudDAO<Person> {

}
