package rws.reference.webmvc.restapi;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.util.UriComponentsBuilder;

import rws.reference.webmvc.restapi.httpexception.BadRequestException;
import rws.reference.webmvc.restapi.httpexception.ConflictException;
import rws.reference.webmvc.restapi.httpexception.MethodNotAllowedException;
import rws.reference.webmvc.restapi.httpexception.NotFoundException;
import rws.reference.webmvc.restapi.httpexception.UnsupportedMediaTypeException;

interface CrudController<K> {
	ResponseEntity<?> create(UriComponentsBuilder uriComponentsBuilder, K resource);

	ResponseEntity<?> update(long id, K resource);

	ResponseEntity<?> delete(long id);

	ResponseEntity<K> get(long id);

	ResponseEntity<?> handleBadRequestException(BadRequestException e);

	ResponseEntity<?> handleNotFoundException(NotFoundException e);

	ResponseEntity<?> handleMethodNotAllowedException(MethodNotAllowedException e);

	ResponseEntity<?> handleConflictException(ConflictException e);

	ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException e);

	ResponseEntity<?> handleUnsupportedMediaTypeException(UnsupportedMediaTypeException e);

	ResponseEntity<?> handleEverythingElse(Exception e);

}
