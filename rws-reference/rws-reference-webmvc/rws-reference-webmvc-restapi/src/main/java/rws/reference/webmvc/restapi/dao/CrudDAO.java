package rws.reference.webmvc.restapi.dao;

public interface CrudDAO<K> {
	long insert(K object);
	K get(long id);
	void update(long id, K object);
	void delete(long id);
}
