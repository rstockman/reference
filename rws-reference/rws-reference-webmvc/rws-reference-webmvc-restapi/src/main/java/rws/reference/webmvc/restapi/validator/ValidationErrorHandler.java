package rws.reference.webmvc.restapi.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import rws.reference.webmvc.restapi.GenericError;
import rws.reference.webmvc.restapi.httpexception.BadRequestException;

public abstract class ValidationErrorHandler {
	protected void handleErrors(Errors errors) {
		if (errors.hasErrors()) {
			List<GenericError> errorList = new ArrayList<GenericError>();
			for (ObjectError error : errors.getAllErrors()) {
				if (error instanceof FieldError) {
					GenericError err = new GenericError();
					err.setRejectedValue(((FieldError) error).getRejectedValue());
					err.setField(((FieldError) error).getField());
					err.setMessage(error.getDefaultMessage());
					errorList.add(err);
				} else {
					GenericError err = new GenericError();
					err.setMessage(error.getDefaultMessage());
					errorList.add(err);
				}
			}
			throw new BadRequestException(errorList);
		}
	}
}
