package rws.reference.webmvc.restapi.httpexception;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import rws.reference.webmvc.restapi.GenericError;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The requested resource was not found.")
public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2180418556813103893L;

	private static final String DEFAULT_MESSAGE = HttpStatus.NOT_FOUND.getReasonPhrase();

	private final List<GenericError> errors;

	public NotFoundException() {
		super(DEFAULT_MESSAGE);
		this.errors = null;
	}

	public NotFoundException(String message) {
		super(message);
		this.errors = null;
	}

	public NotFoundException(Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = null;
	}

	public NotFoundException(String message, Throwable t) {
		super(message, t);
		this.errors = null;
	}

	public NotFoundException(List<GenericError> errors) {
		super(DEFAULT_MESSAGE);
		this.errors = errors;
	}

	public NotFoundException(String message, List<GenericError> errors) {
		super(message);
		this.errors = errors;
	}

	public NotFoundException(List<GenericError> errors, Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = errors;
	}

	public NotFoundException(String message, List<GenericError> errors, Throwable t) {
		super(message, t);
		this.errors = errors;
	}

	public List<GenericError> getErrors() {
		return errors;
	}
}