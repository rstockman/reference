package rws.reference.webmvc.restapi;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import rws.reference.webmvc.restapi.dao.PersonDAO;
import rws.reference.webmvc.restapi.httpexception.ConflictException;
import rws.reference.webmvc.restapi.httpexception.NotFoundException;
import rws.reference.webmvc.restapi.model.Person;
import rws.reference.webmvc.restapi.validator.PersonValidator;

@Controller
@RequestMapping("/person")
class PersonController extends BaseController implements CrudController<Person> {

	@Autowired
	private PersonDAO personDAO;

	@Autowired
	private PersonValidator personValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(personValidator);
	}

	@Override
	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> create(UriComponentsBuilder uriComponentsBuilder, @Valid @RequestBody Person resource) {

		Assert.notNull(resource, "resource null");

		if (resource.getId() != null) {
			GenericError genericError = new GenericError();
			genericError.setMessage("expected person.id=null but was person.id=" + resource.getId());
			List<GenericError> genericErrors = new ArrayList<GenericError>();
			genericErrors.add(genericError);
			throw new ConflictException(genericErrors);
		}

		final long id = personDAO.insert(resource);

		UriComponents uriComponents = uriComponentsBuilder.path("/person/{id}").buildAndExpand(id);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);

	}

	@Override
	@RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, value = "/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @Valid @RequestBody Person resource) {

		Assert.notNull(resource, "resource null");

		if (!Long.valueOf(id).equals(resource.getId())) {
			GenericError genericError = new GenericError();
			genericError.setMessage("expected person.id=" + id + " but was person.id=" + resource.getId());
			List<GenericError> genericErrors = new ArrayList<GenericError>();
			genericErrors.add(genericError);
			throw new ConflictException(genericErrors);
		}

		final Person person = personDAO.get(id);
		if (person == null) {
			GenericError genericError = new GenericError();
			genericError.setMessage("person.id=" + id);
			List<GenericError> genericErrors = new ArrayList<GenericError>();
			genericErrors.add(genericError);
			throw new NotFoundException(genericErrors);
		}
		personDAO.update(id, resource);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@Override
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		personDAO.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, value = "/{id}")
	public ResponseEntity<Person> get(@PathVariable("id") long id) {
		final Person person = personDAO.get(id);
		if (person == null) {
			GenericError genericError = new GenericError();
			genericError.setMessage("person.id=" + id);
			List<GenericError> errors = new ArrayList<GenericError>();
			errors.add(genericError);
			throw new NotFoundException(errors);
		}
		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}

}
