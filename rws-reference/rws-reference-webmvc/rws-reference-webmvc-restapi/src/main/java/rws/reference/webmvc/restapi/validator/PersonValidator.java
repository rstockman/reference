package rws.reference.webmvc.restapi.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import rws.reference.webmvc.restapi.model.Person;

@Component("personValidator")
public class PersonValidator extends ValidationErrorHandler implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Person.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (supports(target.getClass())) {
			handleErrors(errors);
		}
	}

}
