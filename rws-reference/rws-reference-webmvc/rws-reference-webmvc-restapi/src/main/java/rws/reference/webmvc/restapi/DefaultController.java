package rws.reference.webmvc.restapi;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import rws.reference.webmvc.restapi.httpexception.BadRequestException;
import rws.reference.webmvc.restapi.httpexception.MethodNotAllowedException;
import rws.reference.webmvc.restapi.httpexception.NotFoundException;
import rws.reference.webmvc.restapi.httpexception.UnsupportedMediaTypeException;

/**
 * The default controller that will be invoked by Spring any time there isn't a more specific
 * controller.
 * <p>
 * NOTE: The error page mapping is configured in the web.xml
 * </p>
 * @author ross
 */
@Controller
@RequestMapping("/errors")
class DefaultController extends BaseController {

	@RequestMapping(value = "/400")
	public void handleBadRequestException() {
		throw new BadRequestException();
	}

	@RequestMapping(value = "/404")
	public void handleNotFoundException() {
		throw new NotFoundException();
	}

	@RequestMapping(value = "/405")
	public void handleMethodNotAllowedException() {
		throw new MethodNotAllowedException();
	}

	@RequestMapping(value = "/415")
	public void handleUnsupportedMediaTypeException() {
		throw new UnsupportedMediaTypeException();
	}

	@RequestMapping(value = "/{httpCode}")
	public void handleEverythingElse(@PathVariable("httpCode") String httpCode) {
		throw new RuntimeException("Default controller received " + httpCode + " error. Invoking error handler.");
	}

}