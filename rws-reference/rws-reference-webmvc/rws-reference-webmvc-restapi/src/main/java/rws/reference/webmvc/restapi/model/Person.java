package rws.reference.webmvc.restapi.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import rws.reference.webmvc.restapi.utils.CustomJsonTimestampDeserializer;
import rws.reference.webmvc.restapi.utils.CustomJsonTimestampSerializer;
import rws.reference.webmvc.restapi.utils.CustomXmlTimestampAdapter;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "id", "name", "birthday", "married", "occupation", "gender" })
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "name", "birthday", "married", "occupation", "gender" })
public class Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7666136290748967068L;

	@JsonProperty
	@XmlElement
	private Long id;

	@NotNull
	@JsonProperty
	@XmlElement
	private String name;

	@Past
	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	@JsonDeserialize(using = CustomJsonTimestampDeserializer.class)
	@XmlElement
	@XmlJavaTypeAdapter(CustomXmlTimestampAdapter.class)
	private Date birthday;

	@JsonProperty
	@XmlElement
	private Boolean married;

	@JsonProperty
	@XmlElement
	private String occupation;

	@JsonProperty
	@XmlElement
	private String gender;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Boolean getMarried() {
		return married;
	}

	public void setMarried(Boolean married) {
		this.married = married;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}
}
