package rws.reference.webmvc.restapi.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import rws.reference.webmvc.restapi.model.Person;

@Repository
public class PersonDAOImpl implements PersonDAO {

	private static long nextId = 0;

	private static long getNextId() {
		return ++nextId;
	}

	private static final Map<Long, Person> fakeDatabase = new HashMap<Long, Person>();

	@Override
	public long insert(Person object) {

		Assert.notNull(object, "object null");

		long id = getNextId();
		object.setId(id);
		fakeDatabase.put(id, object);
		return id;
	}

	@Override
	public Person get(long id) {
		return fakeDatabase.get(id);
	}

	@Override
	public void update(long id, Person object) {

		Assert.notNull(object, "object null");

		fakeDatabase.put(id, object);
	}

	@Override
	public void delete(long id) {
		fakeDatabase.remove(id);
	}

}
