package rws.reference.webmvc.restapi;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import rws.reference.webmvc.restapi.httpexception.BadRequestException;
import rws.reference.webmvc.restapi.httpexception.ConflictException;
import rws.reference.webmvc.restapi.httpexception.MethodNotAllowedException;
import rws.reference.webmvc.restapi.httpexception.NotFoundException;
import rws.reference.webmvc.restapi.httpexception.UnsupportedMediaTypeException;

abstract class BaseController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<GenericErrorResponse> handleBadRequestException(BadRequestException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.BAD_REQUEST;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setReason(getReason(e));
		genericErrorResponse.setErrors(e.getErrors());

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<GenericErrorResponse> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.BAD_REQUEST;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setReason("The request cannot be fulfilled due to bad syntax.");
		GenericError genericError = new GenericError();
		genericError.setMessage("Message could not be parsed due to bad syntax or incorrect Content-Type header.");
		List<GenericError> genericErrors = new ArrayList<GenericError>();
		genericErrors.add(genericError);
		genericErrorResponse.setErrors(genericErrors);

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<GenericErrorResponse> handleNotFoundException(NotFoundException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.NOT_FOUND;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setReason(getReason(e));
		genericErrorResponse.setErrors(e.getErrors());

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler(MethodNotAllowedException.class)
	public ResponseEntity<GenericErrorResponse> handleMethodNotAllowedException(MethodNotAllowedException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.METHOD_NOT_ALLOWED;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setReason(getReason(e));
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler(ConflictException.class)
	public ResponseEntity<GenericErrorResponse> handleConflictException(ConflictException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.CONFLICT;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setReason(getReason(e));
		genericErrorResponse.setErrors(e.getErrors());

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler(UnsupportedMediaTypeException.class)
	public ResponseEntity<?> handleUnsupportedMediaTypeException(UnsupportedMediaTypeException e) {

		HttpStatus status = getStatus(e);
		if (status == null) {
			status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
		}

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setReason(getReason(e));

		genericErrorResponse.setHttpStatus(status.getReasonPhrase());
		genericErrorResponse.setHttpCode(status.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
				genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
	}

	@ExceptionHandler
	public ResponseEntity<GenericErrorResponse> handleEverythingElse(Exception e) {
		if (e instanceof IllegalArgumentException) {
			return handleIllegalArgumentException((IllegalArgumentException) e);
		} else {

			HttpStatus status = getStatus(e);
			if (status == null) {
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}

			GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
			genericErrorResponse.setReason(getReason(e));
			genericErrorResponse.setHttpStatus(status.getReasonPhrase());
			genericErrorResponse.setHttpCode(status.value());
			genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
			LOGGER.warn("Handling {}: {} >> Returning status code: {}, referenceID: >> {}", new Object[] { status.getReasonPhrase(), e,
					genericErrorResponse.getHttpCode(), genericErrorResponse.getReferenceID() });
			LOGGER.error(genericErrorResponse.getReferenceID() + " CAUSE >> ", e);

			return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, status);
		}
	}

	private ResponseEntity<GenericErrorResponse> handleIllegalArgumentException(IllegalArgumentException e) {
		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setHttpStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
		genericErrorResponse.setHttpCode(HttpStatus.BAD_REQUEST.value());
		genericErrorResponse.setReferenceID(UUID.randomUUID().toString());
		LOGGER.info(
				"Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.BAD_REQUEST.getReasonPhrase(), e, genericErrorResponse.getHttpCode(),
						genericErrorResponse.getReferenceID() });

		return new ResponseEntity<GenericErrorResponse>(genericErrorResponse, HttpStatus.BAD_REQUEST);
	}

	private HttpStatus getStatus(Exception e) {
		if (e.getClass().isAnnotationPresent(ResponseStatus.class) && e.getClass().getAnnotation(ResponseStatus.class).value() != null) {
			return e.getClass().getAnnotation(ResponseStatus.class).value();
		} else {
			return null;
		}
	}

	private String getReason(Exception e) {
		if (e.getClass().isAnnotationPresent(ResponseStatus.class) && e.getClass().getAnnotation(ResponseStatus.class).value() != null) {
			return e.getClass().getAnnotation(ResponseStatus.class).reason();
		} else {
			return null;
		}
	}

}
