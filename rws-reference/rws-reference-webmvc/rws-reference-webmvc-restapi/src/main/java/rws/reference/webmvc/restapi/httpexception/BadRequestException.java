package rws.reference.webmvc.restapi.httpexception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import rws.reference.webmvc.restapi.GenericError;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The request cannot be fulfilled due to bad syntax.")
public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5670025516555692666L;

	private static final String DEFAULT_MESSAGE = HttpStatus.BAD_REQUEST.getReasonPhrase();

	private final List<GenericError> errors;

	public BadRequestException() {
		super(DEFAULT_MESSAGE);
		this.errors = null;
	}

	public BadRequestException(String message) {
		super(message);
		this.errors = null;
	}

	public BadRequestException(Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = null;
	}

	public BadRequestException(String message, Throwable t) {
		super(message, t);
		this.errors = null;
	}

	public BadRequestException(List<GenericError> errors) {
		super(DEFAULT_MESSAGE);
		this.errors = errors;
	}

	public BadRequestException(String message, List<GenericError> errors) {
		super(message);
		this.errors = errors;
	}

	public BadRequestException(List<GenericError> errors, Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = errors;
	}

	public BadRequestException(String message, List<GenericError> errors, Throwable t) {
		super(message, t);
		this.errors = errors;
	}

	public List<GenericError> getErrors() {
		if (errors == null) {
			return new ArrayList<GenericError>();
		} else {
			return errors;
		}
	}
}
