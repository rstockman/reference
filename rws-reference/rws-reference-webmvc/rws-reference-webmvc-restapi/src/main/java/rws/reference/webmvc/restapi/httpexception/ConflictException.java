package rws.reference.webmvc.restapi.httpexception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import rws.reference.webmvc.restapi.GenericError;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "The request could not be completed due to a conflict with the current state of the resource.")
public class ConflictException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7770535535983326149L;

	private static final String DEFAULT_MESSAGE = HttpStatus.CONFLICT.getReasonPhrase();

	private final List<GenericError> errors;

	public ConflictException() {
		super(DEFAULT_MESSAGE);
		this.errors = null;
	}

	public ConflictException(String message) {
		super(message);
		this.errors = null;
	}

	public ConflictException(Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = null;
	}

	public ConflictException(String message, Throwable t) {
		super(message, t);
		this.errors = null;
	}

	public ConflictException(List<GenericError> errors) {
		super(DEFAULT_MESSAGE);
		this.errors = errors;
	}

	public ConflictException(String message, List<GenericError> errors) {
		super(message);
		this.errors = errors;
	}

	public ConflictException(List<GenericError> errors, Throwable t) {
		super(DEFAULT_MESSAGE, t);
		this.errors = errors;
	}

	public ConflictException(String message, List<GenericError> errors, Throwable t) {
		super(message, t);
		this.errors = errors;
	}

	public List<GenericError> getErrors() {
		if (errors == null) {
			return new ArrayList<GenericError>();
		} else {
			return errors;
		}
	}
}