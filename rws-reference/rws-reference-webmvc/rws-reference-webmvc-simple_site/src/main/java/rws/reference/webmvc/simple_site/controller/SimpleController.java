package rws.reference.webmvc.simple_site.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/simple")
public class SimpleController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleController.class);

	@RequestMapping(value = "/example", method = RequestMethod.GET)
	public String getSimpleForm(Model model) {

		populateCommonAttributes(model);
		
		SimpleForm form = new SimpleForm();
		model.addAttribute("form", form);

		return "simple_form";

	}
	
	@RequestMapping(value = "/example", method = RequestMethod.POST)
	public String postSimpleForm(Model model, @ModelAttribute @Valid SimpleForm form, BindingResult result) {

		if (result.hasErrors()) {
			List<String> errors = new ArrayList<String>();
			for (ObjectError error : result.getFieldErrors()) {
				errors.add(error.getDefaultMessage());
			}
			model.addAttribute("errors", errors);
			model.addAttribute("hasErrors", true);
		} else {
			// process the request here, i.e., call autowired service methods, etc.
		}
		
		populateCommonAttributes(model);
		if (form.getBirthdate() != null) {
			form.setFormattedBirthdate(new SimpleDateFormat("MM/dd/yyyy").format(form.getBirthdate()));
		}
		if (form.getGender() != null) {
			if ("male".equals(form.getGender())) {
				form.setMaleChecked(true);
			} else if ("female".equals(form.getGender())) {
				form.setFemaleChecked(true);
			}
		}
		model.addAttribute("form", form);

		return "simple_form";

	}
	
	public static class SimpleForm {
		@NotNull(message = "Name must not be empty.")
		@Size(max = 50, message = "Name is too long.")
		private String name;
		private String occupation;
		private Boolean married;
		private Date birthdate;
		private String formattedBirthdate;
		@Pattern(regexp = "male|female", message = "Invalid gender.")
		private String gender;
		private Boolean maleChecked;
		private Boolean femaleChecked;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getOccupation() {
			return occupation;
		}
		public void setOccupation(String occupation) {
			this.occupation = occupation;
		}
		public Boolean getMarried() {
			return married;
		}
		public void setMarried(Boolean married) {
			this.married = married;
		}
		public Date getBirthdate() {
			return birthdate;
		}
		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}
		public String getFormattedBirthdate() {
			return formattedBirthdate;
		}
		public void setFormattedBirthdate(String formattedBirthdate) {
			this.formattedBirthdate = formattedBirthdate;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public Boolean getMaleChecked() {
			return maleChecked;
		}
		public void setMaleChecked(Boolean maleChecked) {
			this.maleChecked = maleChecked;
		}
		public Boolean getFemaleChecked() {
			return femaleChecked;
		}
		public void setFemaleChecked(Boolean femaleChecked) {
			this.femaleChecked = femaleChecked;
		}
		@Override
		public String toString() {
			StandardToStringStyle style = new StandardToStringStyle();
			style.setFieldSeparator(", ");
			style.setUseIdentityHashCode(false);
			return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
		}
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
}
