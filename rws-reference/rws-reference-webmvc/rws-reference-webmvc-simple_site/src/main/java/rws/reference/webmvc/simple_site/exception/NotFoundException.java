package rws.reference.webmvc.simple_site.exception;

public class NotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3471463379151291573L;
	
	private static final String DEFAULT_STATUS = "Resource not found";

	public NotFoundException() {
		super(DEFAULT_STATUS);
	}
	
	public NotFoundException(String message) {
		super(message);
	}
}
