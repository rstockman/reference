package rws.reference.webmvc.simple_site.exception;

public class BadRequestException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4396080607747264371L;
	
	private static final String DEFAULT_STATUS = "Bad Request";

	public BadRequestException() {
		super(DEFAULT_STATUS);
	}
}
