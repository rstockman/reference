package rws.reference.webmvc.simple_site.exception;

import org.springframework.http.ResponseEntity;

import rws.reference.webmvc.simple_site.model.GenericErrorResponse;

public class RestResponseException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4513797279737713945L;
	
	protected ResponseEntity<GenericErrorResponse> responseEntity;

	public RestResponseException() {
		super();
	}

	public ResponseEntity<GenericErrorResponse> getResponseEntity() {
		return this.responseEntity;
	}

	public void setResponseEntity(ResponseEntity<GenericErrorResponse> responseEntity) {
		this.responseEntity = (ResponseEntity<GenericErrorResponse>) responseEntity;
	}

}
