package rws.reference.webmvc.simple_site.exception;

public class MovedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5009453570719142961L;

	private static final String DEFAULT_STATUS = "Moved Permanently. Location ";

	private String location;

	public MovedException(String location) {
		super(DEFAULT_STATUS + location);
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
}
