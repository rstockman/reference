package rws.reference.webmvc.site_javaconfig.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import rws.reference.webmvc.site_javaconfig.aop.ServiceLoggingAspect;

@Configuration
@ComponentScan(basePackages = { "rws.reference.webmvc.site_javaconfig" })
@EnableAspectJAutoProxy
public class AppConfig {
	
	@Bean
	public PropertyPlaceholderConfigurer placeholderProperties() {
		PropertyPlaceholderConfigurer placeholderProperties = new PropertyPlaceholderConfigurer();
		placeholderProperties.setIgnoreResourceNotFound(true);
		placeholderProperties.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
		placeholderProperties.setLocations(new Resource[] { new ClassPathResource("site_javaconfig.properties") });
		return placeholderProperties;
	}
	
	@Bean
	public ServiceLoggingAspect httpLoggingAspect() {
		return new ServiceLoggingAspect();
	}
	
}