package rws.reference.webmvc.site_javaconfig.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import rws.reference.webmvc.site_javaconfig.exception.BadRequestException;
import rws.reference.webmvc.site_javaconfig.exception.ForbiddenException;
import rws.reference.webmvc.site_javaconfig.exception.MethodNotAllowedException;
import rws.reference.webmvc.site_javaconfig.exception.NotFoundException;
import rws.reference.webmvc.site_javaconfig.exception.UnauthorizedException;

/**
 * The default controller that will be invoked by Spring any time there isn't a more specific
 * controller.
 * <p>
 * NOTE: The error page mapping is configured in the web.xml
 * </p>
 * @author vagrant
 */
@Controller
@RequestMapping("/errors")
public class DefaultController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultController.class);

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@RequestMapping(value = "/400")
	public void handle400() {
		throw new BadRequestException();
	}

	@RequestMapping(value = "/404")
	public void handle404() {
		throw new NotFoundException();
	}

	@RequestMapping(value = "/401")
	public void handle401() {
		throw new UnauthorizedException();
	}

	@RequestMapping(value = "/403")
	public void handle403() {
		throw new ForbiddenException();
	}

	@RequestMapping(value = "/405")
	public void handle405() {
		throw new MethodNotAllowedException();
	}
	
	@RequestMapping(value = "/{httpCode}")
	public void handleEverythingElse(@PathVariable("httpCode") String httpCode) {
		throw new RuntimeException("Default controller received " + httpCode + " error. Invoking error handler.");
	}

}