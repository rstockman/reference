package rws.reference.webmvc.site_javaconfig.exception;

public class UnauthorizedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7040194070133665158L;
	
	private static final String DEFAULT_STATUS = "Authentication is required and has failed or has not yet been provided";

	public UnauthorizedException() {
		super(DEFAULT_STATUS);
	}
	
	public UnauthorizedException(String message) {
		super(message);
	}
}
