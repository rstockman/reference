package rws.reference.webmvc.site_javaconfig.exception;

public class ForbiddenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7156987317562167378L;

	private static final String DEFAULT_STATUS = "The request was a valid request, but the server is refusing to respond to it";

	public ForbiddenException() {
		super(DEFAULT_STATUS);
	}
	
	public ForbiddenException(String message) {
		super(message);
	}
}
