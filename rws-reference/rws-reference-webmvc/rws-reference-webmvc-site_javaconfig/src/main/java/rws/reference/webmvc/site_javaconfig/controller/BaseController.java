package rws.reference.webmvc.site_javaconfig.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import rws.reference.webmvc.site_javaconfig.exception.BadRequestException;
import rws.reference.webmvc.site_javaconfig.exception.CSRFViolationException;
import rws.reference.webmvc.site_javaconfig.exception.ForbiddenException;
import rws.reference.webmvc.site_javaconfig.exception.MethodNotAllowedException;
import rws.reference.webmvc.site_javaconfig.exception.NotFoundException;
import rws.reference.webmvc.site_javaconfig.exception.RestResponseException;
import rws.reference.webmvc.site_javaconfig.exception.UnauthorizedException;
import rws.reference.webmvc.site_javaconfig.model.ErrorView;
import rws.reference.webmvc.site_javaconfig.model.GenericErrorResponse;

public abstract class BaseController {
	
	@Value("${rws.reference.webmvc.simple_site.base_path}")
	private String basePath;
	
	@Value("${http.400.message}")
	private String httpMessage400;

	@Value("${http.401.message}")
	private String httpMessage401;

	@Value("${http.403.message}")
	private String httpMessage403;

	@Value("${http.404.message}")
	private String httpMessage404;

	@Value("${http.405.message}")
	private String httpMessage405;

	@Value("${http.default.message}")
	private String httpMessageDefault;
	
	/**
	 * Retrieves a logger from the subclass so the proper class names is displayed for error
	 * handling
	 * @return
	 */
	protected abstract Logger getLogger();
	
	/**
	 * converts empty form field values to null
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true));
	}
	
	protected void populateCommonAttributes(Model model) {
		model.addAttribute("base_path", basePath);
	}
	
	@ExceptionHandler({IllegalArgumentException.class, BadRequestException.class})
	public ModelAndView handle400(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage400);
		errorView.setHttpCode(HttpStatus.BAD_REQUEST.toString());
		errorView.setHttpMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.BAD_REQUEST.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public ModelAndView handle401(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage401);
		errorView.setHttpCode(HttpStatus.UNAUTHORIZED.toString());
		errorView.setHttpMessage(HttpStatus.UNAUTHORIZED.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.UNAUTHORIZED.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(ForbiddenException.class)
	public ModelAndView handle403(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage403);
		errorView.setHttpCode(HttpStatus.FORBIDDEN.toString());
		errorView.setHttpMessage(HttpStatus.FORBIDDEN.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.FORBIDDEN.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(CSRFViolationException.class)
	public ModelAndView handleCSRF(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage403);
		errorView.setHttpCode(HttpStatus.FORBIDDEN.toString());
		errorView.setHttpMessage(HttpStatus.FORBIDDEN.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		// CSRF logged at warn level
		getLogger().warn("Handling CSRF violation: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ModelAndView handle404(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage404);
		errorView.setHttpCode(HttpStatus.NOT_FOUND.toString());
		errorView.setHttpMessage(HttpStatus.NOT_FOUND.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.UNAUTHORIZED.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(MethodNotAllowedException.class)
	public ModelAndView handle405(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessage405);
		errorView.setHttpCode(HttpStatus.METHOD_NOT_ALLOWED.toString());
		errorView.setHttpMessage(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(HttpClientErrorException.class)
	public ModelAndView handleHttpClientError(HttpClientErrorException e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(e.getMessage());
		errorView.setHttpCode(((HttpClientErrorException) e).getStatusCode().toString());
		errorView.setHttpMessage(((HttpClientErrorException) e).getStatusCode().getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().debug("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { ((HttpClientErrorException) e).getStatusCode().getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		getLogger().error("referenceID: >> " + errorView.getReferenceId() + " Stack Trace:", e);
		return modelAndView;
	}
	
	@ExceptionHandler(RestResponseException.class)
	public ModelAndView handleRestErrors(RestResponseException e) {
		ResponseEntity<GenericErrorResponse> errorResponse = e.getResponseEntity();
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessageDefault);
		errorView.setHttpCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		errorView.setHttpMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().error("Handling {} returned from REST server: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { errorResponse.getStatusCode().getReasonPhrase(), e, HttpStatus.INTERNAL_SERVER_ERROR, errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler
	public ModelAndView handleEverythingElse(Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		ErrorView errorView = new ErrorView();
		errorView.setErrorMessage(httpMessageDefault);
		errorView.setHttpCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		errorView.setHttpMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		errorView.setReferenceId(UUID.randomUUID().toString());
		getLogger().error("Handling {}: {} >> Returning status code: {}, referenceID: >> {}",
				new Object[] { HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), e, errorView.getHttpCode(), errorView.getReferenceId() });
		modelAndView.addObject("base_path", basePath);
		modelAndView.addObject("error", errorView);
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}
}
