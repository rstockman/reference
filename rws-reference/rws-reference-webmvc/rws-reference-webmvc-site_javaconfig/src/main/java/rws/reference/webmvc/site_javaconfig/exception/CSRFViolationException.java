package rws.reference.webmvc.site_javaconfig.exception;

public class CSRFViolationException extends ForbiddenException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2813811302144930268L;
	
	private static final String DEFAULT_STATUS = "Access is denied";

	public CSRFViolationException() {
		super(DEFAULT_STATUS);
	}
	
	public CSRFViolationException(String message) {
		super(message);
	}
}
