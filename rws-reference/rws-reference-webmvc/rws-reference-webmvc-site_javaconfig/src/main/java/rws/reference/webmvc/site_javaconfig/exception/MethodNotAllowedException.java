package rws.reference.webmvc.site_javaconfig.exception;

public class MethodNotAllowedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7905416912357673289L;

	private static final String DEFAULT_STATUS = "A request was made of a resource using a request method not supported by that resource";

	public MethodNotAllowedException() {
		super(DEFAULT_STATUS);
	}
	
	public MethodNotAllowedException(String message) {
		super(message);
	}
}
