package rws.reference.webmvc.site_javaconfig.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class ServiceLoggingAspect {

	@Pointcut("within(@org.springframework.stereotype.Service *) || within(@org.springframework.stereotype.Controller *) || within(@org.springframework.stereotype.Component *)")
	public void serviceBean() {
	}

	@Pointcut("execution(* *(..))")
	public void methodPointcut() {
	}

	@Before("serviceBean() && methodPointcut()")
	public void logBefore(JoinPoint joinPoint) {
		Logger logger = getLog(joinPoint);
		if (logger.isDebugEnabled()) {
			logger.debug("The method " + joinPoint.getSignature().getName() + "() begins"
					+ (joinPoint.getArgs().length > 0 ? (" with " + Arrays.toString(joinPoint.getArgs())) : ""));
		}
	}

	@AfterReturning(pointcut = "serviceBean() && methodPointcut()", returning = "result")
	public void logAfter(JoinPoint joinPoint, Object result) {
		Logger logger = getLog(joinPoint);
		if (logger.isDebugEnabled()) {
			logger.debug("The method " + joinPoint.getSignature().getName() + "() ends with " + result);
		}
	}

	@AfterThrowing(pointcut = "serviceBean() && methodPointcut()", throwing = "t")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable t) throws Throwable {
		Logger logger = getLog(joinPoint);
		logger.error("An exception " + t + " has been thrown in " + joinPoint.getSignature().getName() + "()");
		throw t;
	}

	private Logger getLog(JoinPoint joinPoint) {
		return LoggerFactory.getLogger(joinPoint.getTarget().getClass());
	}

}