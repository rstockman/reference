package rws.reference.webmvc.site_javaconfig.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rws.reference.webmvc.site_javaconfig.exception.NotFoundException;

@Controller
@RequestMapping("/list")
public class ListController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListController.class);
	
	private static final Map<String, List<ListItem>> lists = new HashMap<String, List<ListItem>>();
	
	static {
		List<ListItem> occupations = new ArrayList<ListItem>();
		ListItem item1 = new ListItem();
		item1.setLabel("Tester");
		item1.setValue("Tester");
		item1.setSelected(false);
		occupations.add(item1);
		ListItem item2 = new ListItem();
		item2.setLabel("Programmer");
		item2.setValue("Programmer");
		item2.setSelected(false);
		occupations.add(item2);
		ListItem item3 = new ListItem();
		item3.setLabel("User");
		item3.setValue("User");
		item3.setSelected(false);
		occupations.add(item3);
		lists.put("occupations", occupations);
	}
	
	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
	
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String getList(Model model, @PathVariable("name") String name, @RequestParam(value = "selected", required = false) String selected) {
		List<ListItem> list = lists.get(name);
		if (list == null) {
			throw new NotFoundException();
		}
		if (selected != null) {
			for (ListItem item : list) {
				if (selected.equals(item.getValue())) {
					item.setSelected(true);
				}
			}
		}
		model.addAttribute("list", list);

		return "item_list";

	}
	
	public static class ListItem {
		private String label;
		private String value;
		private Boolean selected;
		
		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Boolean getSelected() {
			return selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}

		@Override
		public String toString() {
			StandardToStringStyle style = new StandardToStringStyle();
			style.setFieldSeparator(", ");
			style.setUseIdentityHashCode(false);
			return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
		}
	}

}
