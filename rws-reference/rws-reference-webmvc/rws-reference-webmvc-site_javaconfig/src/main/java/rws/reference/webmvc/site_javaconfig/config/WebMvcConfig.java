package rws.reference.webmvc.site_javaconfig.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;
import org.springframework.web.servlet.view.mustache.MustacheTemplateLoader;
import org.springframework.web.servlet.view.mustache.MustacheViewResolver;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "rws.reference.webmvc.site_javaconfig" })
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Bean
	public ViewResolver viewResolver(ResourceLoader resourceLoader) {
		MustacheViewResolver viewResolver = new MustacheViewResolver();
		viewResolver.setCache(false);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".mustache");
		MustacheTemplateLoader mustacheTemplateLoader = new MustacheTemplateLoader();
		mustacheTemplateLoader.setResourceLoader(resourceLoader);
		viewResolver.setTemplateLoader(mustacheTemplateLoader);
		return viewResolver;
	}

	@Bean
	public DefaultHandlerExceptionResolver defaultHandlerExceptionResolver() {
		return new DefaultHandlerExceptionResolver();
	}

}
