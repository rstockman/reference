package rws.reference.parser.example.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("phoneNumber")
@JsonPropertyOrder({ "type", "phoneNumber" })
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "phoneNumber", propOrder = { "type", "phoneNumber" })
@XmlRootElement(name = "phoneNumber")
public class PhoneNumber extends Identifiable implements Serializable {

	private static final long serialVersionUID = 6361447729599933942L;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Phone number type is required")
	@Size(min = 1, max = 15, message = "Phone number type must have between 1 and 15 characters")
	private String type;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Phone number is required")
	@Pattern(regexp = "^\\d{3}-\\d{3}-\\d{4}$", message = "Required format is ###-###-####")
	private String phoneNumber;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}
