package rws.reference.parser.example.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("address")
@JsonPropertyOrder({ "type", "addressLine1", "addressLine2", "city", "state", "zipcode", "country" })
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "address", propOrder = { "type", "addressLine1", "addressLine2", "city", "state", "zipcode", "country" })
@XmlRootElement(name = "address")
public class Address extends Identifiable implements Serializable {

	private static final long serialVersionUID = -88596082731725109L;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Address type is required")
	@Size(min = 1, max = 15, message = "Address type must have between 1 and 15 characters")
	private String type;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Address line 1 is required")
	@Size(min = 1, max = 15, message = "Address line 1 must have between 1 and 50 characters")
	private String addressLine1;

	@JsonProperty
	@XmlElement
	@Size(min = 1, max = 50, message = "Address line 2 must have between 1 and 50 characters")
	private String addressLine2;

	@JsonProperty
	@XmlElement
	@NotNull(message = "City is required")
	@Size(min = 1, max = 50, message = "City must have between 1 and 50 characters")
	private String city;

	@JsonProperty
	@XmlElement
	@NotNull(message = "State is required")
	@Size(min = 1, max = 50, message = "State must have between 1 and 50 characters")
	private String state;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Zipcode is required")
	@Pattern(regexp = "^\\d{5}$", message = "Zipcode must contain 5 digits")
	private String zipcode;

	@JsonProperty
	@XmlElement
	@Size(min = 1, max = 50, message = "Country must have between 1 and 50 characters")
	private String country;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}
