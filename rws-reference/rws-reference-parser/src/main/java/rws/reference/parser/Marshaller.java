package rws.reference.parser;

/**
 * Abstract class for marshaling an object into a serialized format such as a string or input
 * stream.
 * @author ross
 * @param <T> Java serializable object to marshal the java object to
 */
public abstract class Marshaller<T> {

	/**
	 * Unmarshals a serialized object into a java object
	 * @param input
	 * @param type
	 * @return
	 */
	protected abstract <K> K unmarshal(T input, Class<K> type);

	/**
	 * Marshals a java object into a serialized format
	 * @param input
	 * @return
	 */
	protected abstract T marshal(Object input);

	protected final boolean prettyPrint;

	/**
	 * Constructor providing option to serialized document structure using new lines and indents
	 * @param prettyPrint
	 */
	protected Marshaller(boolean prettyPrint) {
		this.prettyPrint = prettyPrint;
	}

	/**
	 * Default constructor. Formatted document structure is disabled by default
	 */
	protected Marshaller() {
		this(false);
	}
}
