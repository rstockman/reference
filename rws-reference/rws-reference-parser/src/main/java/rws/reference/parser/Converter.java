package rws.reference.parser;

/**
 * Interface to convert an object into another object
 * @author ross
 * @param <T> Output type
 * @param <K> Input type
 */
public interface Converter<K, T> {
	/**
	 * Convert an object from one type to another type
	 * @param input type
	 * @return Output type
	 */
	T convert(K input);
}
