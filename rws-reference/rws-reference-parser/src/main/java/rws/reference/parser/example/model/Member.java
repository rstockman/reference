package rws.reference.parser.example.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("member")
@JsonPropertyOrder({ "id", "firstName", "lastName", "dateOfBirth", "married", "children", "phoneNumbers", "addresses", "memberLevel" })
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "member", propOrder = { "id", "firstName", "lastName", "dateOfBirth", "married", "children", "phoneNumbers", "addresses",
		"memberLevel" })
@XmlRootElement(name = "member")
public class Member extends Identifiable implements Serializable {

	private static final long serialVersionUID = 156354490043279290L;

	@JsonProperty
	@XmlElement
	@NotNull(message = "First name is required")
	@Size(min = 1, max = 50, message = "First name must have between 1 and 50 characters")
	private String firstName;

	@JsonProperty
	@XmlElement
	@NotNull(message = "Last name is required")
	@Size(min = 1, max = 50, message = "Last name must have between 1 and 50 characters")
	private String lastName;

	@JsonProperty
	@XmlElement
	@Past(message = "Date of firth must be in the past")
	private Date dateOfBirth;

	@JsonProperty
	@XmlElement
	private Boolean married;

	@JsonProperty
	@XmlElement(name = "child")
	@XmlElementWrapper(name = "children")
	@Valid
	private List<Member> children;

	@JsonProperty
	@XmlElement(name = "phoneNumber")
	@XmlElementWrapper(name = "phoneNumbers")
	@Valid
	@Size(max = 3, message = "Cannot have more than 3 phone numbers")
	private List<PhoneNumber> phoneNumbers;

	@JsonProperty
	@XmlElement(name = "address")
	@XmlElementWrapper(name = "addresses")
	@Valid
	@Size(min = 1, max = 3, message = "Must have at least 1 address but no more than 3")
	private List<Address> addresses;

	@JsonProperty
	@XmlElement
	@Min(value = 1, message = "Member level cannot be less than 1")
	@Max(value = 15, message = "Member level cannot be greater than 15")
	private Long memberLevel;

	public final String getFirstName() {
		return firstName;
	}

	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public final String getLastName() {
		return lastName;
	}

	public final void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public final Date getDateOfBirth() {
		return dateOfBirth;
	}

	public final void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public final Boolean getMarried() {
		return married;
	}

	public final void setMarried(Boolean married) {
		this.married = married;
	}

	public final List<Member> getChildren() {
		return children;
	}

	public final void setChildren(List<Member> children) {
		this.children = children;
	}

	public final List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public final void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public final List<Address> getAddresses() {
		return addresses;
	}

	public final void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public final Long getMemberLevel() {
		return memberLevel;
	}

	public final void setMemberLevel(Long memberLevel) {
		this.memberLevel = memberLevel;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}
