package rws.reference.parser;

import java.io.IOException;
import java.io.StringWriter;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 * Marshals a java object into a String formatted into a JSON structure
 * @author ross
 * @param <S>
 */
public abstract class JsonMarshaller<S> extends Marshaller<String> implements GenericStringMarshaller<S> {

	/**
	 * @param prettyPrint If true then marshaled JSON structure will be formatted with new lines and
	 *        indentation. This is intended for human readability. Otherwise marshal output will not
	 *        be formatted.
	 */
	protected JsonMarshaller(boolean prettyPrint) {
		super(prettyPrint);
	}

	protected JsonMarshaller() {
		super();
	}

	@Override
	protected final <K> K unmarshal(String input, Class<K> type) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(input, type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public final String marshal(Object input) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			if (prettyPrint) {
				mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
			}
			StringWriter stringWriter = new StringWriter();
			mapper.writeValue(stringWriter, input);
			return stringWriter.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
