package rws.reference.parser.example;

import rws.reference.parser.XmlMarshaller;
import rws.reference.parser.example.model.Member;

public class CustomerXmlMarshaller extends XmlMarshaller<Member> {

	public CustomerXmlMarshaller(boolean prettyPrint) {
		super(prettyPrint);
	}

	public CustomerXmlMarshaller() {
		super();
	}

	@Override
	public final Member unmarshal(String input) {
		return unmarshal(input, Member.class);
	}

}
