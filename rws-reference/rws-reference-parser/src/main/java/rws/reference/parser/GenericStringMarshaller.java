package rws.reference.parser;

/**
 * Interface for marshalling an object into a document structure such as json or xml and
 * unmarshalling a document structure into a java object. The document structure will be in String
 * form.
 * @author ross
 * @param <S>
 */
public interface GenericStringMarshaller<S> {

	/**
	 * Marshal an object into a textual document structure
	 * @param input object
	 * @return textual document structure
	 */
	String marshal(Object input);

	/**
	 * Unmarshals a textual document structure into a java object
	 * @param input textual document structure
	 * @return java object
	 */
	S unmarshal(String input);
}
