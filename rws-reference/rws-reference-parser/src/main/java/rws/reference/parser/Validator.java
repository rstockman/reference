package rws.reference.parser;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.StandardToStringStyle;

/**
 * Validation interface for validating a java object.
 * @author ross
 */
public interface Validator {
	/**
	 * Validates a java object and returns a list of violations found, if any. If the resulting list
	 * is empty then the object can be assumed to be valid. This method must not return null.
	 * @param obj
	 * @return List of violations, if any.
	 * @see rws.reference.parser.Validator$Violation
	 */
	List<Violation> validate(Object obj);

	/**
	 * Violation container to hold details of a validation violation.
	 * @author ross
	 */
	class Violation implements Serializable {

		private static final long serialVersionUID = 9199629788309824905L;

		private String field;

		private Object value;

		private String message;

		/**
		 * Protected constructor. This class should not be instantiated outside of the Validation
		 * interface implementation.
		 * @param field
		 * @param value
		 * @param message
		 */
		protected Violation(String field, Object value, String message) {
			this.field = field;
			this.value = value;
			this.message = message;
		}

		public final String getField() {
			return field;
		}

		public final Object getValue() {
			return value;
		}

		public final String getMessage() {
			return message;
		}

		@Override
		public String toString() {
			StandardToStringStyle style = new StandardToStringStyle();
			style.setFieldSeparator(", ");
			style.setUseIdentityHashCode(false);
			return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
		}
	}
}
