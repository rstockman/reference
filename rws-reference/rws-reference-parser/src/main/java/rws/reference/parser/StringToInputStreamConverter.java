package rws.reference.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Converter to transform a String into an InputStream
 * @author ross
 */
public class StringToInputStreamConverter implements Converter<String, InputStream> {

	@Override
	public InputStream convert(String input) {
		return new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
	}

}
