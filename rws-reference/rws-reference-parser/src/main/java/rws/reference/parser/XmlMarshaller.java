package rws.reference.parser;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;

/**
 * Marshals a java object into a String formatted into a xml structure
 * @author ross
 * @param <S>
 */
public abstract class XmlMarshaller<S> extends Marshaller<String> implements GenericStringMarshaller<S> {

	/**
	 * @param prettyPrint If true then marshaled xml structure will be formatted with new lines and
	 *        indentation. This is intended for human readability. Otherwise marshal output will not
	 *        be formatted.
	 */
	protected XmlMarshaller(boolean prettyPrint) {
		super(prettyPrint);
	}

	protected XmlMarshaller() {
		super();
	}

	@Override
	protected <K> K unmarshal(String input, Class<K> type) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(type);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			return unmarshaller.unmarshal(XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(input)), type).getValue();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public final String marshal(Object input) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(input.getClass());
			javax.xml.bind.Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			if (prettyPrint) {
				marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			}
			StringWriter stringWriter = new StringWriter();
			marshaller.marshal(input, stringWriter);
			return stringWriter.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
