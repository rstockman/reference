package rws.reference.parser.example;

import rws.reference.parser.JsonMarshaller;
import rws.reference.parser.example.model.Member;

public class CustomerJsonMarshaller extends JsonMarshaller<Member> {

	public CustomerJsonMarshaller(boolean prettyPrint) {
		super(prettyPrint);
	}

	public CustomerJsonMarshaller() {
		super();
	}

	@Override
	public final Member unmarshal(String input) {
		return unmarshal(input, Member.class);
	}

}
