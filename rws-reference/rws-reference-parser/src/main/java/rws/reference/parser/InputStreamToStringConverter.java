package rws.reference.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Converter to transform an InputStream into a String
 * @author ross
 */
public class InputStreamToStringConverter implements Converter<InputStream, String> {

	private final String newLine;

	public InputStreamToStringConverter() {
		newLine = System.getProperty("line.separator");
	}

	@Override
	public String convert(InputStream input) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			StringBuilder output = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line);
				output.append(newLine);
			}
			return output.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
