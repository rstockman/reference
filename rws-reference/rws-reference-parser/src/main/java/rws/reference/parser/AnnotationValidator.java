package rws.reference.parser;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 * Implementation of validator that validates a java object by reading field annotations from
 * javax.validation package.
 * @author ross
 */
public class AnnotationValidator implements Validator {

	@Override
	public List<Violation> validate(Object obj) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		List<Violation> violations = new ArrayList<Violation>();
		for (ConstraintViolation<Object> violation : factory.getValidator().validate(obj)) {
			violations.add(new Violation(violation.getPropertyPath().toString(), violation.getInvalidValue(), violation.getMessage()));
		}
		return violations;
	}

}
