package rws.reference.parser.example;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;

import rws.reference.parser.AnnotationValidator;
import rws.reference.parser.GenericStringMarshaller;
import rws.reference.parser.Validator.Violation;
import rws.reference.parser.example.model.Address;
import rws.reference.parser.example.model.Member;
import rws.reference.parser.example.model.PhoneNumber;

public class Main {

	private static final Long DATE_OF_BIRTH = System.currentTimeMillis();

	public static void main(String[] args) throws FileNotFoundException {

		test(getCustomer1(), new GenericStringMarshaller<?>[] { new CustomerJsonMarshaller(true), new CustomerXmlMarshaller(true) });

		AnnotationValidator validator = new AnnotationValidator();
		for (Violation violation : validator.validate(getCustomer1())) {
			System.out.println(violation);
		}

	}

	public static void test(Object customer, GenericStringMarshaller<?>[] marshallers) {
		System.out.println(customer);
		for (GenericStringMarshaller<?> marshaller : marshallers) {
			String str = marshaller.marshal(customer);
			System.out.println(str);
			customer = marshaller.unmarshal(str);
		}
		System.out.println(customer);
	}

	public static Member getCustomer1() {
		Member customer = new Member();
		customer.setFirstName("");
		customer.setDateOfBirth(new Date(DATE_OF_BIRTH));
		customer.setAddresses(new ArrayList<Address>());
		customer.getAddresses().add(new Address());
		customer.setMarried(false);
		customer.setPhoneNumbers(new ArrayList<PhoneNumber>());
		customer.setId(1L);
		customer.setChildren(new ArrayList<Member>());
		customer.getChildren().add(new Member());
		customer.getChildren().get(0).setLastName("Bobby");
		customer.getChildren().get(0).setId(2L);
		customer.getChildren().get(0).setPhoneNumbers(new ArrayList<PhoneNumber>());
		customer.getChildren().get(0).getPhoneNumbers().add(new PhoneNumber());
		customer.getChildren().get(0).getPhoneNumbers().get(0).setId(7L);
		customer.getChildren().get(0).getPhoneNumbers().get(0).setPhoneNumber("457-842-1578");
		customer.getChildren().get(0).getPhoneNumbers().get(0).setType("Home");
		customer.getChildren().get(0).getPhoneNumbers().add(new PhoneNumber());
		customer.getChildren().get(0).getPhoneNumbers().get(1).setId(9L);
		customer.getChildren().get(0).getPhoneNumbers().get(1).setPhoneNumber("4578424758");
		customer.getChildren().get(0).getPhoneNumbers().get(1).setType("Mobile");
		customer.getChildren().add(new Member());
		customer.getChildren().get(1).setLastName("Bonnie");
		customer.getChildren().get(1).setId(3L);
		customer.getChildren().get(1).setChildren(new ArrayList<Member>());
		customer.getChildren().get(1).getChildren().add(new Member());
		customer.getChildren().get(1).getChildren().get(0).setFirstName("Jackie");
		customer.getChildren().get(1).getChildren().get(0).setLastName("Dent");
		customer.getChildren().get(1).getChildren().get(0).setMemberLevel(27L);
		customer.getChildren().get(1).getChildren().get(0).setMarried(true);
		customer.getChildren().get(1).getChildren().get(0).setId(4L);
		return customer;
	}
}
