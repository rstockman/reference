package rws.reference.mockito.sample;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.util.ReflectionTestUtils;

import rws.reference.mockito.sample.DateConverter;
import rws.reference.mockito.sample.DateValidationServiceImpl;

public class MockWithVoidMethodTest {

	@Mock
	private DateConverter dateConverter;

	@InjectMocks
	private DateValidationServiceImpl systemUnderTest;

	@Before
	public void init() {
		// initialize the mocks
		MockitoAnnotations.initMocks(this);

		// since systemUnderTest has a private field that has no public setter we can use this
		// spring-test utility to set that value via reflection.
		ReflectionTestUtils.setField(systemUnderTest, "format", "yyyy-MM-dd");
	}

	@Test
	public void testMockedVoidMethod() {
		String input = "2014-01-13";

		// since the validate method on dateConverter returns void we don't need to mock anything
		// UNLESS the void method is supposed to modify the passed in object in some way. In this
		// case it does not.
		boolean result = systemUnderTest.validateFormat(input);

		Assert.assertTrue("Must return true for valid dates", result);
	}

	@Test
	public void testMockedVoidMethodThrowsException() {
		String input = "2014/01/13";

		// since the validate method on dateConverter returns void we don't need to mock anything
		// UNLESS the void method is supposed to modify the passed in object in some way. In this
		// case it does not. However in this case we are passing an illegal date and we want to test
		// that systemUnderTest can handle exceptions thrown by the validate method on
		// dateConverter.
		Mockito.doThrow(new IllegalArgumentException()).when(dateConverter).validate(Mockito.anyString(), Mockito.anyString());

		boolean result = systemUnderTest.validateFormat(input);

		Assert.assertFalse("Must return false for invalid dates", result);
	}

	@Test
	public void testMockedVoidMethodModifyInput() {

		// sometimes void methods make changes to the input object. Use this technique to mock that
		// behavior.
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				((Calendar) args[0]).set(Calendar.HOUR_OF_DAY, 0);
				((Calendar) args[0]).set(Calendar.MINUTE, 0);
				((Calendar) args[0]).set(Calendar.SECOND, 0);
				((Calendar) args[0]).set(Calendar.MILLISECOND, 0);
				return null;
			}

		}).when(dateConverter).setToMidnight(Mockito.any(Calendar.class));

		Calendar result = systemUnderTest.getCalendarWithoutTime();

		Assert.assertNotNull("Calendar result must not be null", result);
		Assert.assertEquals("Hour of day should be 0", 0, result.get(Calendar.HOUR_OF_DAY));
		Assert.assertEquals("Minutes should be 0", 0, result.get(Calendar.MINUTE));
		Assert.assertEquals("Seconds should be 0", 0, result.get(Calendar.SECOND));
		Assert.assertEquals("Milliseconds should be 0", 0, result.get(Calendar.MILLISECOND));
	}

	@Test
	public void testInputsPassedInToAVoidMethod() {

		// sometimes systemUnderTest may perform some logic on an object before passing it into a
		// void method. You can put assertions inside the doAnswer like this to validate the
		// expected inputs got passed into the mocked void method.
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();

				Calendar current = Calendar.getInstance();

				Assert.assertNotNull("Calendar result must not be null", (Calendar) args[0]);
				Assert.assertEquals("Year must be current year", current.get(Calendar.YEAR), ((Calendar) args[0]).get(Calendar.YEAR));
				Assert.assertEquals("Month must be current month", current.get(Calendar.MONTH), ((Calendar) args[0]).get(Calendar.MONTH));
				Assert.assertEquals("Day of month must be current day", current.get(Calendar.DAY_OF_MONTH), ((Calendar) args[0]).get(Calendar.DAY_OF_MONTH));

				((Calendar) args[0]).set(Calendar.HOUR_OF_DAY, 0);
				((Calendar) args[0]).set(Calendar.MINUTE, 0);
				((Calendar) args[0]).set(Calendar.SECOND, 0);
				((Calendar) args[0]).set(Calendar.MILLISECOND, 0);
				return null;
			}

		}).when(dateConverter).setToMidnight(Mockito.any(Calendar.class));

		// since the validate method on dateConverter returns void we don't need to mock anything
		// UNLESS the void method is supposed to modify the passed in object in some way. In this
		// case it does not.
		Calendar result = systemUnderTest.getCalendarWithoutTime();

		Assert.assertNotNull("Calendar result must not be null", result);
		Assert.assertEquals("Hour of day should be 0", 0, result.get(Calendar.HOUR_OF_DAY));
		Assert.assertEquals("Minutes should be 0", 0, result.get(Calendar.MINUTE));
		Assert.assertEquals("Seconds should be 0", 0, result.get(Calendar.SECOND));
		Assert.assertEquals("Milliseconds should be 0", 0, result.get(Calendar.MILLISECOND));
	}

}
