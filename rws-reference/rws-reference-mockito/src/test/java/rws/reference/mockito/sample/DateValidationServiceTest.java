package rws.reference.mockito.sample;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rws.reference.mockito.sample.DateConverter;
import rws.reference.mockito.sample.DateValidationServiceImpl;

public class DateValidationServiceTest {

	@Mock
	private DateConverter dateConverter;

	@InjectMocks
	private DateValidationServiceImpl systemUnderTest;

	@Before
	public void init() {
		// initialize the mocks
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testValidDate() throws ParseException {

		String input = "2014-01-01T00:00:00.000-0500";

		// remember the scope of this test is to test the DateValidationServiceImpl. So the
		// functionality if dateConverter does not matter. We only care that the systemUnderTest is
		// able to properly process the output of the dateConverter and that systemUnderTest
		// returns the correct result to the user
		Mockito.when(dateConverter.convert(Mockito.anyString())).thenReturn(new Date(System.currentTimeMillis() - 1));

		Date result = systemUnderTest.validatePastDate(input);

		Assert.assertNotNull("Must return a date", result);
		Assert.assertTrue("Date must be in the past", result.before(new Date()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFutureDate() throws ParseException {

		String input = "2019-01-01T00:00:00.000-0500";

		// remember the scope of this test is to test the DateValidationServiceImpl. So the
		// functionality if dateConverter does not matter. We only care that the systemUnderTest is
		// able to properly process the output of the dateConverter and that systemUnderTest
		// returns the correct result to the user. In this case the dateConverter will return a date
		// that is not in the past and we need to test that systemUnderTest throws
		// IllegalArgumentException
		Mockito.when(dateConverter.convert(Mockito.anyString())).thenReturn(new Date(System.currentTimeMillis() + 1));

		systemUnderTest.validatePastDate(input);
		Assert.fail("Expected IllegalArgumentException");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNull() throws ParseException {

		String input = null;

		// remember the scope of this test is to test the DateValidationServiceImpl. So the
		// functionality if dateConverter does not matter. We only care that the systemUnderTest is
		// able to properly process the output of the dateConverter and that systemUnderTest
		// returns the correct result to the user. In this case the dateConverter will throw an
		// IllegalArgumentException when the input is null.
		Mockito.when(dateConverter.convert(null)).thenThrow(new IllegalArgumentException());

		systemUnderTest.validatePastDate(input);
		Assert.fail("Expected IllegalArgumentException");
	}

	@Test
	public void testInvalidFormat() throws ParseException {

		String input = "not a date";

		// remember the scope of this test is to test the DateValidationServiceImpl. So the
		// functionality if dateConverter does not matter. We only care that the systemUnderTest is
		// able to properly process the output of the dateConverter and that systemUnderTest
		// returns the correct result to the user. In this case the dateConverter will throw a
		// ParseException and we need to test that the systemUnderTest was able to handle it
		// properly.
		Mockito.when(dateConverter.convert(Mockito.anyString())).thenThrow(new ParseException("error", 0));

		try {
			systemUnderTest.validatePastDate(input);
		} catch (Exception e) {
			Assert.assertEquals("Expected IllegalArgumentException", IllegalArgumentException.class, e.getClass());
		}

	}
}
