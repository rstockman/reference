package rws.reference.mockito.sample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.ResponseEntity;

public class SpringControllerTest {
	@Mock
	private DateValidationService dateValidationService;

	@InjectMocks
	private DateController systemUnderTest;

	@Mock
	// most servlet related inputs to controller methods can be mocked as you see in this example,
	// we need to call the addCookie(..) method
	private HttpServletResponse httpServietResponse;

	@Before
	public void init() {
		// initialize the mocks
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testResonseContainsCookie() throws ParseException {
		final String inputDate = "2014-01-13";
		final String cookieDate = "2014-01-12";

		Mockito.when(dateValidationService.validatePastDate(inputDate)).thenReturn(getDate(inputDate));
		Mockito.when(dateValidationService.validatePastDate(cookieDate)).thenReturn(getDate(cookieDate));

		// check the value set in the cookie on the response object
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();

				Assert.assertNotNull("Cookie must not be null", (Cookie) args[0]);
				Assert.assertEquals("Cookie must match input as it was greater", inputDate, ((Cookie) args[0]).getValue());
				Assert.assertEquals("Cookie name must be date", "date", ((Cookie) args[0]).getName());
				return null;
			}

		}).when(httpServietResponse).addCookie(Mockito.any(Cookie.class));

		ResponseEntity<Date> result = systemUnderTest.checkDate(inputDate, cookieDate, httpServietResponse);

		Assert.assertNotNull("Body must not be null", result.getBody());
		Assert.assertEquals("Input and output date should match", inputDate, new SimpleDateFormat("yyyy-MM-dd").format(result.getBody()));

	}

	@Test
	public void testResonseDoesNotContainCookie() throws ParseException {
		final String inputDate = "2014-01-13";
		final String cookieDate = null;

		Mockito.when(dateValidationService.validatePastDate(inputDate)).thenReturn(getDate(inputDate));

		// check the value set in the cookie on the response object
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();

				Assert.assertNotNull("Cookie must not be null", (Cookie) args[0]);
				Assert.assertEquals("Cookie must match input as it was greater", inputDate, ((Cookie) args[0]).getValue());
				Assert.assertEquals("Cookie name must be date", "date", ((Cookie) args[0]).getName());
				return null;
			}

		}).when(httpServietResponse).addCookie(Mockito.any(Cookie.class));

		ResponseEntity<Date> result = systemUnderTest.checkDate(inputDate, cookieDate, httpServietResponse);

		Assert.assertNotNull("Body must not be null", result.getBody());
		Assert.assertEquals("Input and output date should match", inputDate, new SimpleDateFormat("yyyy-MM-dd").format(result.getBody()));

	}

	@Test
	public void testResonseCookieDateGreater() throws ParseException {
		final String inputDate = "2014-01-12";
		final String cookieDate = "2014-01-13";

		Mockito.when(dateValidationService.validatePastDate(inputDate)).thenReturn(getDate(inputDate));
		Mockito.when(dateValidationService.validatePastDate(cookieDate)).thenReturn(getDate(cookieDate));

		// check the value set in the cookie on the response object
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();

				Assert.assertNotNull("Cookie must not be null", (Cookie) args[0]);
				Assert.assertEquals("Cookie must match input as it was greater", cookieDate, ((Cookie) args[0]).getValue());
				Assert.assertEquals("Cookie name must be date", "date", ((Cookie) args[0]).getName());
				return null;
			}

		}).when(httpServietResponse).addCookie(Mockito.any(Cookie.class));

		ResponseEntity<Date> result = systemUnderTest.checkDate(inputDate, cookieDate, httpServietResponse);

		Assert.assertNotNull("Body must not be null", result.getBody());
		Assert.assertEquals("Input and output date should match", inputDate, new SimpleDateFormat("yyyy-MM-dd").format(result.getBody()));

	}

	private Date getDate(String str) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(str);
	}

}
