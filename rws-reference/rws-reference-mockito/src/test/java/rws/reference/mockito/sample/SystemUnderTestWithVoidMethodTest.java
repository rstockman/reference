package rws.reference.mockito.sample;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SystemUnderTestWithVoidMethodTest {
	@Mock
	private DateConverter dateConverter;

	@InjectMocks
	private DateValidationServiceImpl systemUnderTest;

	@Before
	public void init() {
		// initialize the mocks
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testVoidMethod() {
		// The problem here is the method we are testing is void so there are no results to perform
		// assertions on.

		final int yearInput = 2014;
		final int monthInput = 0;
		final int dayOfMonthInput = 13;

		// To get around this problem we can use a mock on a class that is called within the void
		// method and validate the inputs passed in to verify they were set properly within the
		// void. In this case when the void method calls setToMidnight(..) we can add assertions to
		// validate that the calendar passed in to this method has the year, month, and day set
		// properly that were passed in as inputs to the method we are testing.
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();

				Assert.assertNotNull("Calendar result must not be null", (Calendar) args[0]);
				Assert.assertEquals("Year must be passed in year", yearInput, ((Calendar) args[0]).get(Calendar.YEAR));
				Assert.assertEquals("Month must be passed in month", monthInput, ((Calendar) args[0]).get(Calendar.MONTH));
				Assert.assertEquals("Day of month must be passed in day", dayOfMonthInput, ((Calendar) args[0]).get(Calendar.DAY_OF_MONTH));

				// mock behavior
				((Calendar) args[0]).set(Calendar.HOUR_OF_DAY, 0);
				((Calendar) args[0]).set(Calendar.MINUTE, 0);
				((Calendar) args[0]).set(Calendar.SECOND, 0);
				((Calendar) args[0]).set(Calendar.MILLISECOND, 0);
				return null;
			}

		}).when(dateConverter).setToMidnight(Mockito.any(Calendar.class));

		// run the test
		systemUnderTest.validateDate(yearInput, monthInput, dayOfMonthInput);
	}
}
