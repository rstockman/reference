package rws.reference.mockito.sample;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DateValidationServiceImpl implements DateValidationService {

	@Autowired
	private DateConverter dateConverter;

	@Value("${date_format}")
	private String format;

	@Override
	public Date validatePastDate(String date) {
		try {
			Date convertedDate = dateConverter.convert(date);
			if (convertedDate.before(new Date(System.currentTimeMillis()))) {
				return convertedDate;
			} else {
				throw new IllegalArgumentException("not a past date");
			}
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public boolean validateFormat(String date) {
		try {
			dateConverter.validate(date, format);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public Calendar getCalendarWithoutTime() {
		Calendar calendar = Calendar.getInstance();
		dateConverter.setToMidnight(calendar);
		return calendar;
	}

	@Override
	public void validateDate(int year, int month, int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

		dateConverter.setToMidnight(calendar);

		// insert into database, etc.
	}

}
