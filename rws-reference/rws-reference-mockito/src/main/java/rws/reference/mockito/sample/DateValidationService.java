package rws.reference.mockito.sample;

import java.util.Calendar;
import java.util.Date;

public interface DateValidationService {
	/**
	 * Converts a string into a date and validates that it is a PAST date.
	 * @param date
	 * @return
	 * @throws IllegalArgumentException if invalid date
	 */
	Date validatePastDate(String date);

	/**
	 * Returns true if the date format is valid.
	 * @param date
	 * @return
	 * @throws IllegalArgumentException if invalid date
	 */
	boolean validateFormat(String date);

	/**
	 * Returns a calendar with time set to midnight in current timezone
	 * @return
	 */
	Calendar getCalendarWithoutTime();

	/**
	 * Creates a date using the passed in values, then validates the resulting date.
	 * @param year
	 * @param month
	 * @param dayOfMonth
	 * @throws IllegalArgumentException if not a valid date
	 */
	void validateDate(int year, int month, int dayOfMonth);
}
