package rws.reference.mockito.sample;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public interface DateConverter {

	/**
	 * Converts a string into a date type. Expected format yyyy-MM-dd'T'HH:mm:ss.SSSZ. Throws parse
	 * exception if unable to parse or if input is null
	 * @param date
	 * @return
	 * @throws ParseException if unable to parse date.
	 * @throws IllegalArgumentException if the input is null.
	 */
	Date convert(String date) throws ParseException;

	/**
	 * Checks if the date string matches the format provided. Throws IllegalArgumentException if
	 * not.
	 * @param date
	 * @param validFormat
	 * @throws IllegalArgumentException if invalid
	 */
	void validate(String date, String validFormat);

	/**
	 * Takes the provided calendar and sets the time to midnight in the current timezone
	 * @param calendar
	 */
	void setToMidnight(Calendar calendar);
}
