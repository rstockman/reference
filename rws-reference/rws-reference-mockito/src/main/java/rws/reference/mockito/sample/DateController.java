package rws.reference.mockito.sample;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/test")
public class DateController {

	@Autowired
	private DateValidationService dateValidationService;

	@RequestMapping(value = "/{date}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<Date> checkDate(@PathVariable("date") String date, @CookieValue("date") String lastDate,
			HttpServletResponse response) {
		Date outputDate = dateValidationService.validatePastDate(date);
		if (lastDate != null && outputDate.before(dateValidationService.validatePastDate(lastDate))) {
			response.addCookie(new Cookie("date", lastDate));
		} else {
			response.addCookie(new Cookie("date", date));
		}

		return new ResponseEntity<Date>(outputDate, HttpStatus.OK);
	}
}
