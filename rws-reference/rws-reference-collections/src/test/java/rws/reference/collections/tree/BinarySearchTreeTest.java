package rws.reference.collections.tree;

import org.junit.Assert;
import org.junit.Test;

import rws.reference.collections.list.BinarySearchTree;
import rws.reference.collections.list.BinarySearchTree.Node;

public class BinarySearchTreeTest {

	@Test
	public void testInsert() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Assert.assertTrue("Tree should be empty", tree.isEmpty());
		tree.insert(1);
		Assert.assertFalse("Tree should not be emtpy", tree.isEmpty());
		Node<Integer> node = tree.find(1);
		validateStructure(node);
		Assert.assertEquals("Node key should equal inserted key", Integer.valueOf(1), node.getKey());
		validateNasNoChildren(node);
	}

	@Test
	public void testInsertTwoEqual() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Assert.assertTrue("Tree should be empty", tree.isEmpty());
		tree.insert(1);
		tree.insert(1);
		Assert.assertFalse("Tree should not be emtpy", tree.isEmpty());
		Node<Integer> node = tree.find(1);
		validateStructure(node);
		Assert.assertEquals("Node key should equal inserted key", Integer.valueOf(1), node.getKey());
		validateHasNoLeftChild(node);
		Node<Integer> rightChild = validateRightChild(node, 1);
		validateNasNoChildren(rightChild);

	}

	@Test
	public void testInsertGreater() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Assert.assertTrue("Tree should be empty", tree.isEmpty());
		tree.insert(1);
		tree.insert(2);
		Assert.assertFalse("Tree should not be emtpy", tree.isEmpty());
		Node<Integer> node = tree.find(1);
		validateStructure(node);
		Assert.assertEquals("Node key should equal inserted key", Integer.valueOf(1), node.getKey());
		validateHasNoLeftChild(node);
		Node<Integer> rightChild = validateRightChild(node, 2);
		validateNasNoChildren(rightChild);
	}

	@Test
	public void testInsertLess() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Assert.assertTrue("Tree should be empty", tree.isEmpty());
		tree.insert(2);
		tree.insert(1);
		Assert.assertFalse("Tree should not be emtpy", tree.isEmpty());
		Node<Integer> node = tree.find(2);
		validateStructure(node);
		Assert.assertEquals("Node key should equal inserted key", Integer.valueOf(2), node.getKey());
		validateHasNoRightChild(node);
		Node<Integer> rightChild = validateLeftChild(node, 1);
		validateNasNoChildren(rightChild);
	}

	private void validateStructure(Node<Integer> node) {
		Assert.assertNotNull("Node must not have null keys", node.getKey());
		if (node.getLeft() != null) {
			Assert.assertNotNull("Left child node must not have null keys", node.getLeft().getKey());
			Assert.assertTrue("Left child node must be less than current", node.getLeft().getKey().compareTo(node.getKey()) < 0);
		}
		if (node.getRight() != null) {
			Assert.assertNotNull("Right child node must not have null keys", node.getRight().getKey());
			Assert.assertTrue("Right child node must be equal to or greater than current", node.getRight().getKey()
					.compareTo(node.getKey()) >= 0);
		}
	}

	private void validateHasNoLeftChild(Node<Integer> node) {
		Assert.assertNull("Left child node should be null", node.getLeft());
	}

	private void validateHasNoRightChild(Node<Integer> node) {
		Assert.assertNull("Right child node should be null", node.getRight());
	}

	private void validateNasNoChildren(Node<Integer> node) {
		validateHasNoLeftChild(node);
		validateHasNoRightChild(node);
	}

	private void validateHasLeftChild(Node<Integer> node) {
		Assert.assertNotNull("Left child node should not be null", node.getLeft());
	}

	private void validateHasRightChild(Node<Integer> node) {
		Assert.assertNotNull("Right child node should not be null", node.getRight());
	}

	private Node<Integer> validateRightChild(Node<Integer> node, Integer key) {
		validateStructure(node);
		validateHasRightChild(node);
		Assert.assertEquals("Right child must equal value " + key, key, node.getRight().getKey());
		return node.getRight();
	}

	private Node<Integer> validateLeftChild(Node<Integer> node, Integer key) {
		validateStructure(node);
		validateHasLeftChild(node);
		Assert.assertEquals("Left child must equal value " + key, key, node.getLeft().getKey());
		return node.getLeft();
	}

	@Test
	public void testInserts() {

		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();

		tree.insert(1);
		tree.printInorder();
		System.out.println();
		tree.delete(2);
		tree.printInorder();
		System.out.println();

		tree.insert(16);
		tree.insert(8);
		tree.insert(24);
		tree.insert(4);
		tree.insert(12);
		tree.insert(20);
		tree.insert(28);
		tree.insert(2);
		tree.insert(6);
		tree.insert(10);
		tree.insert(14);
		tree.insert(18);
		tree.insert(22);
		tree.insert(26);
		tree.insert(30);
		tree.insert(1);
		tree.insert(3);
		tree.insert(5);
		tree.insert(7);
		tree.insert(9);
		tree.insert(11);
		tree.insert(13);
		tree.insert(15);
		tree.insert(17);
		tree.insert(19);
		tree.insert(21);
		tree.insert(23);
		tree.insert(25);
		tree.insert(27);
		tree.insert(29);
		tree.insert(31);

		tree.printInorder();

		tree.delete(24);

		System.out.println();

		tree.printInorder();

		System.out.println();

		System.out.println(tree.find(24));

	}
}
