package rws.reference.collections.list;

public class BinarySearchTree<K extends Comparable<K>> {

	private Node<K> root;

	public BinarySearchTree() {
		root = null;
	}

	public boolean isEmpty() {
		return root == null;
	}

	public void delete(K key) {
		if (!isEmpty()) {
			Node<K> parent = root;
			Node<K> current = parent;
			while (current != null && !key.equals(current.getKey())) {
				if (key.compareTo(current.getKey()) < 0) {
					parent = current;
					current = current.getLeft();
				} else {
					parent = current;
					current = current.getRight();
				}
			}
			// if the parent and current are the same object then deleting the root node.
			if (parent == current) {
				root = null;
			} else if (current != null) {
				Node<K> orphanedLeft = current.getLeft();
				Node<K> orphanedRight = current.getRight();
				if (current.getKey().compareTo(parent.getKey()) < 0 && orphanedLeft != null) {
					parent.setLeft(orphanedLeft);
					// TODO: traverse tree from current node to find correct location of
					// orphanedRight child
					recursiveInsert(orphanedRight, root, root);
				} else if (current.getKey().compareTo(parent.getKey()) < 0) {
					parent.setRight(orphanedRight);
				} else if (current.getKey().compareTo(parent.getKey()) >= 0 && orphanedLeft != null) {
					parent.setRight(orphanedLeft);
					// TODO: traverse tree from current node to find correct location of
					// orphanedRight child
					recursiveInsert(orphanedRight, root, root);
				} else if (current.getKey().compareTo(parent.getKey()) >= 0) {
					parent.setRight(orphanedRight);
				}
			}
		}
	}

	public Node<K> find(K key) {
		if (isEmpty()) {
			return null;
		}
		Node<K> current = root;
		while (current != null && key.compareTo(current.getKey()) != 0) {
			if (key.compareTo(current.getKey()) < 0) {
				current = current.getLeft();
			} else {
				current = current.getRight();
			}
		}
		return current;
	}

	public void insert(K key) {
		recursiveInsert(new Node<K>(key), root, root);
	}

	private void recursiveInsert(Node<K> node, Node<K> parent, Node<K> current) {
		if (parent == null) {
			root = node;
		} else if (current == null) {
			if (node.getKey().compareTo(parent.getKey()) < 0) {
				parent.setLeft(node);
			} else {
				parent.setRight(node);
			}
		} else if (node.getKey().compareTo(current.getKey()) < 0) {
			parent = current;
			current = current.getLeft();
			recursiveInsert(node, parent, current);
		} else {
			parent = current;
			current = current.getRight();
			recursiveInsert(node, parent, current);
		}
	}

	public void printInorder() {
		traverseInorder(root);
	}

	public void traverseInorder(Node<K> node) {
		if (node != null) {
			traverseInorder(node.getLeft());
			System.out.println(node.getKey());
			traverseInorder(node.getRight());
		}
	}

	public static final class Node<T extends Comparable<T>> {
		private Node<T> right;

		private Node<T> left;

		private T key;

		Node(T key) {
			setKey(key);
		}

		public Node<T> getRight() {
			return right;
		}

		void setRight(Node<T> right) {
			this.right = right;
		}

		public Node<T> getLeft() {
			return left;
		}

		void setLeft(Node<T> left) {
			this.left = left;
		}

		public T getKey() {
			return key;
		}

		void setKey(T key) {
			this.key = key;
		}

		public String toString() {
			return key.toString();
		}

	}
}
