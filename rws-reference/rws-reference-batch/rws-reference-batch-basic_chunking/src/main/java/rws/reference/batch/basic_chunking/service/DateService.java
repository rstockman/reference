package rws.reference.batch.basic_chunking.service;

import java.util.Date;

public interface DateService {
	String getDate(long id);

	void writeDate(Date date);
}
