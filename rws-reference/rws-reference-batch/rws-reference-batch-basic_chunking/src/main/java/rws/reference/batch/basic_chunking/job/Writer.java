package rws.reference.batch.basic_chunking.job;

import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemWriter;

import rws.reference.batch.basic_chunking.service.DateService;

public class Writer implements ItemWriter<Date> {

	private DateService dateService;

	public Writer(DateService dateService) {
		this.dateService = dateService;
	}

	@Override
	public void write(List<? extends Date> items) throws Exception {
		for (Date item : items) {
			dateService.writeDate(item);
		}
	}

}
