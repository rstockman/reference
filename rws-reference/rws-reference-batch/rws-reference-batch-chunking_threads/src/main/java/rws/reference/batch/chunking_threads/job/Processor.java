package rws.reference.batch.chunking_threads.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class Processor implements ItemProcessor<String, Date> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class);

	private static final Date INVALID_DATE = null;

	@Override
	public Date process(String item) throws Exception {
		if (item.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d")) {
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(item);
			if (item.equals(new SimpleDateFormat("yyyy-MM-dd").format(date))) {
				return date;
			}
		}
		LOGGER.info("Invalid date >> {}", item);
		return INVALID_DATE;
	}

}
