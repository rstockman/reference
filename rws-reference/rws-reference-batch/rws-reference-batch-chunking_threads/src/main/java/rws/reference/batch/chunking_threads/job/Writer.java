package rws.reference.batch.chunking_threads.job;

import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemWriter;

import rws.reference.batch.chunking_threads.service.DateService;

public class Writer implements ItemWriter<Date> {

	private DateService dateService;

	public Writer(DateService dateService) {
		this.dateService = dateService;
	}

	@Override
	public void write(List<? extends Date> items) throws Exception {
		for (Date item : items) {
			dateService.writeDate(item);
		}
	}

}
