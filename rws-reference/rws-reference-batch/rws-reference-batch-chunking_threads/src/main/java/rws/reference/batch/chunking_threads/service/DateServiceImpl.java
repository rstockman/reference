package rws.reference.batch.chunking_threads.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DateServiceImpl implements DateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateServiceImpl.class);

	private static final Map<Long, String> storedDates;

	static {
		storedDates = new HashMap<Long, String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2329107732035650721L;
			{
				put(1l, "2000-01-16");
				put(2l, "2000-01-23");
				put(3l, "2000-01-07");
				put(4l, "2000-01-01");
				put(5l, "2000-01-08");
				put(6l, "2000-01-24");
				put(7l, "2000-01-14");
				put(8l, "2000-01-28");
				put(9l, "2000-01-31");
				put(10l, "2000-02-24");
				put(11l, "2000-02-25");
				put(12l, "2000-02-29");
				put(13l, "2000-02-17");
				put(14l, "2000-02-08");
				put(15l, "2000-03-09");
				put(16l, "2000-03-23");
				put(17l, "2000-03-36");
				put(18l, "2000-03-23");
				put(19l, "2000-03-15");
				put(20l, "2000-03-31");
				put(21l, "2000-03-32");
				put(22l, "2000-04-26");
				put(23l, "2000-04-30");
				put(24l, "2000-05-26");
				put(25l, "2000-05-10");
				put(26l, "2000-05-13");
				put(27l, "2000-05-26");
				put(28l, "2000-06-13");
				put(29l, "2000-06-25");
				put(30l, "2000-06-13");
				put(31l, "2000-07-25");
				put(32l, "2000-07-07");
				put(33l, "2000-07-07");
				put(34l, "2000-07-09");
				put(35l, "2000-07-23");
				put(36l, "2000-08-30");
				put(37l, "2000-09-31");
				put(38l, "2000-09-23");
				put(39l, "2000-09-16");
				put(40l, "2000-09-04");
				put(41l, "2000-10-06");
				put(42l, "2000-10-23");
				put(43l, "2000-11-15");
				put(44l, "2000-12-31");
				put(45l, "2000-12-23");
				put(46l, "2000-12-12");
				put(47l, "2000-12-05");
				put(48l, "2000-12-04");
				put(49l, "2000-12-08");
				put(50l, "2000-12-26");
				put(51l, "2000-12-31");
				put(52l, "2000-13-19");
			}
		};
	}

	@Override
	public String getDate(long id) {
		String date = storedDates.get(id);
		LOGGER.info("Reading date >> {}", date);
		return date;
	}

	@Override
	public void writeDate(Date date) {
		LOGGER.info("Writting date >> {}", date);
	}

}
