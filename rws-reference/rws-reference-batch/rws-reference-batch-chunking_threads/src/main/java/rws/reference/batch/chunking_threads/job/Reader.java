package rws.reference.batch.chunking_threads.job;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import rws.reference.batch.chunking_threads.service.DateService;

public class Reader implements ItemReader<String> {

	private static final String READER_FINISHED = null;

	private long nextId = 1l;

	private DateService dateService;

	public Reader(DateService dateService) {
		this.dateService = dateService;
	}

	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		long currentId;
		synchronized (this) {
			currentId = nextId++;
		}

		String date = dateService.getDate(currentId);

		if (date != null) {
			return date;
		} else {
			return READER_FINISHED;
		}
	}

}
