package rws.reference.batch.chunking_threads.service;

import java.util.Date;

public interface DateService {
	String getDate(long id);

	void writeDate(Date date);
}
