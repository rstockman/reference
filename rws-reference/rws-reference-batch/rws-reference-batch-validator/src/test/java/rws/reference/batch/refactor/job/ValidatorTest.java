package rws.reference.batch.refactor.job;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/jobs/launcher.xml", "/jobs/validator-job.xml", "/job-runner-context.xml" })
public class ValidatorTest {

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;

	@Test
	public void testCustomJobWithValidParameters() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(new JobParametersBuilder().addString("date", "2000-02-28")
				.toJobParameters());
		Assert.assertEquals("Job must complete with status COMPLETED", BatchStatus.COMPLETED, jobExecution.getStatus());
	}

	@Test(expected = JobParametersInvalidException.class)
	public void testCustomJobWithInvalidParameters() throws Exception {
		jobLauncherTestUtils.launchJob(new JobParametersBuilder().addString("date", "2001-02-29").toJobParameters());
		Assert.fail("expected JobParametersInvalidException");
	}

	@Test(expected = JobParametersInvalidException.class)
	public void testCustomJobWithNullParameters() throws Exception {
		jobLauncherTestUtils.launchJob(new JobParametersBuilder().toJobParameters());
		Assert.fail("expected JobParametersInvalidException");
	}

}
