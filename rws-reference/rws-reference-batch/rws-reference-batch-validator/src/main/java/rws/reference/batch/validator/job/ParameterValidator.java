package rws.reference.batch.validator.job;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.JobParametersValidator;

public class ParameterValidator implements JobParametersValidator {

	private static final String DATE_FORMAT = "yyyy-MM-dd";

	@Override
	public void validate(JobParameters parameters) throws JobParametersInvalidException {
		try {
			String date = parameters.getString("date");
			if (date == null) {
				throw new JobParametersInvalidException("date null!");
			}
			String checkDate = new SimpleDateFormat(DATE_FORMAT).format(new SimpleDateFormat(DATE_FORMAT).parse(date));
			if (!date.equals(checkDate)) {
				throw new JobParametersInvalidException("Invalid date format!");
			}
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}

	}

}
