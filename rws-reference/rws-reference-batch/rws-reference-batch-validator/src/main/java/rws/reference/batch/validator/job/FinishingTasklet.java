package rws.reference.batch.validator.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class FinishingTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(FinishingTasklet.class);

	public RepeatStatus execute(StepContribution step, ChunkContext chunk) throws Exception {
		LOGGER.info("Finishing job...");
		return RepeatStatus.FINISHED;
	}

}
