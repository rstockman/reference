package rws.reference.batch.concurrent_steps.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class ProcessingTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessingTasklet.class);

	public RepeatStatus execute(StepContribution step, ChunkContext chunk) throws Exception {
		for (int i = 0; i < 10; i++) {
			LOGGER.info("Processing...{}", i);
			Thread.sleep(10);
		}
		return RepeatStatus.FINISHED;
	}

}
