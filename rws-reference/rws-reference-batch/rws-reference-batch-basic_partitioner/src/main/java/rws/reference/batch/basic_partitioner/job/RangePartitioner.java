package rws.reference.batch.basic_partitioner.job;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import rws.reference.batch.basic_partitioner.service.DateService;

public class RangePartitioner implements Partitioner {

	private DateService dateService;

	public RangePartitioner(DateService dateService) {
		this.dateService = dateService;
	}

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		final int count = dateService.getCount();
		int targetSize = count / gridSize + 1;

		Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();
		int number = 0;
		int start = 1;
		int end = start + targetSize - 1;

		while (start <= count) {
			ExecutionContext value = new ExecutionContext();
			result.put("partition" + number, value);
			value.putString("name", "Thread" + number);

			if (end >= count) {
				end = count;
			}
			value.putLong("minValue", start);
			value.putLong("maxValue", end);
			start += targetSize;
			end += targetSize;
			number++;
		}

		return result;
	}
}
