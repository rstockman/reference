package rws.reference.batch.basic_partitioner.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import rws.reference.batch.basic_partitioner.service.DateService;

public class ProcessingTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessingTasklet.class);

	private DateService dateService;

	private final long first;

	private final long last;

	public ProcessingTasklet(DateService dateService, long first, long last) {
		this.dateService = dateService;
		this.first = first;
		this.last = last;
	}

	public RepeatStatus execute(StepContribution step, ChunkContext chunk) throws Exception {
		LOGGER.info("Processing range >> {} to {}", first, last);
		for (long i = first; i <= last; i++) {
			LOGGER.info("Reading date >> {}", dateService.getDate(i));
		}
		return RepeatStatus.FINISHED;
	}

}
