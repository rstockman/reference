package rws.reference.batch.api.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.JobParametersInvalidException;

import rws.reference.batch.api.model.GenericError;

public class ParamValidationException extends JobParametersInvalidException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7859267199543144439L;

	private static final String DEFAULT_MESSAGE = "Job parameters invalid.";

	private final List<GenericError> errors;

	public ParamValidationException() {
		super(DEFAULT_MESSAGE);
		this.errors = null;
	}

	public ParamValidationException(String message) {
		super(message);
		this.errors = null;
	}

	public ParamValidationException(List<GenericError> errors) {
		super(DEFAULT_MESSAGE);
		this.errors = errors;
	}

	public ParamValidationException(String message, List<GenericError> errors) {
		super(message);
		this.errors = errors;
	}

	public List<GenericError> getErrors() {
		if (errors == null) {
			return new ArrayList<GenericError>();
		} else {
			return errors;
		}
	}
}
