package rws.reference.batch.api.httpexception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED, reason = "A request was made of a resource using a request method not supported by that resource.")
public class MethodNotAllowedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8562557238480704754L;

	private static final String DEFAULT_MESSAGE = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();

	public MethodNotAllowedException() {
		super(DEFAULT_MESSAGE);
	}

	public MethodNotAllowedException(String message) {
		super(message);
	}

	public MethodNotAllowedException(Throwable t) {
		super(DEFAULT_MESSAGE, t);
	}

	public MethodNotAllowedException(String message, Throwable t) {
		super(message, t);
	}
}