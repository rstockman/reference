package rws.reference.batch.api.httpexception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE, reason = "The server refused this request because the request entity is in a format not supported by the requested resource for the requested method.")
public class UnsupportedMediaTypeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6472308377879174166L;

	private static final String DEFAULT_MESSAGE = HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase();

	public UnsupportedMediaTypeException() {
		super(DEFAULT_MESSAGE);
	}

	public UnsupportedMediaTypeException(String message) {
		super(message);
	}

	public UnsupportedMediaTypeException(Throwable t) {
		super(DEFAULT_MESSAGE, t);
	}

	public UnsupportedMediaTypeException(String message, Throwable t) {
		super(message, t);
	}
}