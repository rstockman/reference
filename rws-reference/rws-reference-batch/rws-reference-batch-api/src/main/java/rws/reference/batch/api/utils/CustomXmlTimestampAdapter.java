package rws.reference.batch.api.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.UnmarshalException;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CustomXmlTimestampAdapter extends XmlAdapter<String, Date> {

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	@Override
	public String marshal(Date date) throws Exception {
		return new SimpleDateFormat(DATE_FORMAT).format(date);
	}

	@Override
	public Date unmarshal(String date) throws Exception {
		try {
			return new SimpleDateFormat(DATE_FORMAT).parse(date);
		} catch (ParseException e) {
			throw new UnmarshalException(e);
		}
	}
}
