package rws.reference.batch.api.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import rws.reference.batch.api.utils.CustomJsonTimestampSerializer;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "stepName", "status", "exitStatus", "exitDescription", "readCount", "writeCount", "commitCount", "rollbackCount",
		"readSkipCount", "processSkipCount", "writeSkipCount", "startTime", "endTime", "lastUpdated", "terminateOnly", "filterCount",
		"failureExceptions" })
public class BatchStep {

	@JsonProperty
	private String stepName;

	@JsonProperty
	private String status;

	@JsonProperty
	private String exitStatus;

	@JsonProperty
	private String exitDescription;

	@JsonProperty
	private Integer readCount;

	@JsonProperty
	private Integer writeCount;

	@JsonProperty
	private Integer commitCount;

	@JsonProperty
	private Integer rollbackCount;

	@JsonProperty
	private Integer readSkipCount;

	@JsonProperty
	private Integer processSkipCount;

	@JsonProperty
	private Integer writeSkipCount;

	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	private Date startTime;

	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	private Date endTime;

	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	private Date lastUpdated;

	@JsonProperty
	private Boolean terminateOnly;

	@JsonProperty
	private Integer filterCount;

	@JsonProperty
	private List<String> failureExceptions;

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExitStatus() {
		return exitStatus;
	}

	public void setExitStatus(String exitStatus) {
		this.exitStatus = exitStatus;
	}

	public String getExitDescription() {
		return exitDescription;
	}

	public void setExitDescription(String exitDescription) {
		this.exitDescription = exitDescription;
	}

	public Integer getReadCount() {
		return readCount;
	}

	public void setReadCount(Integer readCount) {
		this.readCount = readCount;
	}

	public Integer getWriteCount() {
		return writeCount;
	}

	public void setWriteCount(Integer writeCount) {
		this.writeCount = writeCount;
	}

	public Integer getCommitCount() {
		return commitCount;
	}

	public void setCommitCount(Integer commitCount) {
		this.commitCount = commitCount;
	}

	public Integer getRollbackCount() {
		return rollbackCount;
	}

	public void setRollbackCount(Integer rollbackCount) {
		this.rollbackCount = rollbackCount;
	}

	public Integer getReadSkipCount() {
		return readSkipCount;
	}

	public void setReadSkipCount(Integer readSkipCount) {
		this.readSkipCount = readSkipCount;
	}

	public Integer getProcessSkipCount() {
		return processSkipCount;
	}

	public void setProcessSkipCount(Integer processSkipCount) {
		this.processSkipCount = processSkipCount;
	}

	public Integer getWriteSkipCount() {
		return writeSkipCount;
	}

	public void setWriteSkipCount(Integer writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Boolean getTerminateOnly() {
		return terminateOnly;
	}

	public void setTerminateOnly(Boolean terminateOnly) {
		this.terminateOnly = terminateOnly;
	}

	public Integer getFilterCount() {
		return filterCount;
	}

	public void setFilterCount(Integer filterCount) {
		this.filterCount = filterCount;
	}

	public List<String> getFailureExceptions() {
		return failureExceptions;
	}

	public void setFailureExceptions(List<String> failureExceptions) {
		this.failureExceptions = failureExceptions;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}
