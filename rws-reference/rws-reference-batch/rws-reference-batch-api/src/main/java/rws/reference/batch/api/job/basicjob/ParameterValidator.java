package rws.reference.batch.api.job.basicjob;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersValidator;

import rws.reference.batch.api.model.GenericError;
import rws.reference.batch.api.validator.ParamValidationException;

public class ParameterValidator implements JobParametersValidator {

	private static final String DATE_FORMAT = "yyyy-MM-dd";

	private static final String DATE_REGEX = "^\\d\\d\\d\\d-\\d\\d-\\d\\d$";

	@Override
	public void validate(JobParameters parameters) throws ParamValidationException {
		try {

			List<GenericError> errors = new ArrayList<GenericError>();

			String date = parameters.getString("date");
			if (date == null) {
				GenericError error = new GenericError();
				error.setField("date");
				error.setMessage("Cannot be null");
				errors.add(error);
			} else if (!date.matches(DATE_REGEX)
					|| !date.equals(new SimpleDateFormat(DATE_FORMAT).format(new SimpleDateFormat(DATE_FORMAT).parse(date)))) {
				GenericError error = new GenericError();
				error.setField("date");
				error.setMessage("Invalid date.");
				error.setRejectedValue(date);
				errors.add(error);
			}

			if (errors.size() > 0) {
				throw new ParamValidationException("Validation errors", errors);
			}

		} catch (ParseException e) {
			throw new RuntimeException(e);
		}

	}

}
