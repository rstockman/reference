package rws.reference.batch.api.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import rws.reference.batch.api.httpexception.BadRequestException;
import rws.reference.batch.api.httpexception.ConflictException;
import rws.reference.batch.api.httpexception.NotFoundException;
import rws.reference.batch.api.model.BatchExecution;
import rws.reference.batch.api.model.BatchInput;
import rws.reference.batch.api.model.BatchJobParameter;
import rws.reference.batch.api.model.BatchStep;
import rws.reference.batch.api.model.GenericError;
import rws.reference.batch.api.validator.ParamValidationException;

@Controller
@RequestMapping("/jobs")
public class BatchController extends BaseController {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobExplorer jobExplorer;

	@Autowired
	@Qualifier("basicjob")
	private Job basicjob;

	@RequestMapping(value = "/basicjob", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public ResponseEntity<BatchExecution> startContactStagingJob(UriComponentsBuilder builder, @RequestBody BatchInput input)
			throws Exception {

		try {
			List<Long> runningInstances = getRunningInstances(basicjob);
			if (runningInstances.size() == 0) {
				BatchExecution response = convert(jobLauncher.run(basicjob, buildJobParameters(input)));
				return new ResponseEntity<BatchExecution>(response, getHeaders(response.getId(), basicjob.getName(), builder),
						HttpStatus.ACCEPTED);
			} else {
				handleConfilct(basicjob, runningInstances);
				return null;
			}
		} catch (ParamValidationException e) {
			throw new BadRequestException(e.getErrors());
		}

	}

	@RequestMapping(value = "/{jobName}/{id}", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
	public ResponseEntity<BatchExecution> status(@PathVariable("id") long id) {
		JobExecution jobExecution = jobExplorer.getJobExecution(id);
		if (jobExecution == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<BatchExecution>(convert(jobExplorer.getJobExecution(id)), HttpStatus.OK);
	}

	private HttpHeaders getHeaders(long executionId, String jobName, UriComponentsBuilder builder) {
		UriComponents uriComponents = builder.path("/jobs/{jobName}/{id}").buildAndExpand(jobName, executionId);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		return headers;
	}

	private JobParameters buildJobParameters(BatchInput input) {
		JobParametersBuilder builder = new JobParametersBuilder().addLong("time", System.currentTimeMillis());
		if (input.getParameters() != null) {
			for (BatchJobParameter param : input.getParameters()) {
				builder.addString(param.getName(), param.getValue());
			}
		}
		return builder.toJobParameters();
	}

	private List<Long> getRunningInstances(Job... jobs) {
		List<Long> runningInstances = new ArrayList<Long>();
		for (Job job : jobs) {
			for (JobExecution exec : jobExplorer.findRunningJobExecutions(job.getName())) {
				runningInstances.add(exec.getJobId());
			}
		}
		return runningInstances;
	}

	private void handleConfilct(Job job, List<Long> runningInstances) {
		GenericError error = new GenericError();
		error.setMessage("Job " + job.getName() + " cannot be started due to conflict with running instances >> "
				+ runningInstances.toString());
		List<GenericError> errors = new ArrayList<GenericError>();
		errors.add(error);
		throw new ConflictException(errors);
	}

	private BatchExecution convert(JobExecution jobExecution) {
		BatchExecution execution = new BatchExecution();
		execution.setJobStatus(jobExecution.getStatus().toString());
		execution.setName(jobExecution.getJobInstance().getJobName());
		execution.setId(jobExecution.getId());
		execution.setStarted(jobExecution.getCreateTime());
		execution.setEnded(jobExecution.getEndTime());
		if (execution.getStarted() != null && execution.getEnded() != null) {
			execution.setDurationMillis(execution.getEnded().getTime() - execution.getStarted().getTime());
		} else if (execution.getStarted() != null) {
			execution.setDurationMillis(System.currentTimeMillis() - execution.getStarted().getTime());
		}
		JobParameters params = jobExecution.getJobParameters();
		execution.setParameters(new ArrayList<BatchJobParameter>());
		if (params != null && params.getParameters() != null) {
			for (Entry<String, JobParameter> entry : params.getParameters().entrySet()) {
				final String paramKey = entry.getKey();
				final Object paramValue = entry.getValue().getValue();
				execution.getParameters().add(new BatchJobParameter(paramKey, paramValue == null ? null : paramValue.toString()));
			}
		}

		if (jobExecution.getExitStatus() != null) {
			execution.setExitStatus(jobExecution.getExitStatus().getExitCode());
		}

		Collection<StepExecution> steps = jobExecution.getStepExecutions();

		if (steps != null) {
			execution.setSteps(new ArrayList<BatchStep>());
			for (StepExecution step : steps) {
				BatchStep batchStep = new BatchStep();
				if (step.getFailureExceptions() != null) {
					batchStep.setFailureExceptions(new ArrayList<String>());
					for (Throwable t : step.getFailureExceptions()) {
						batchStep.getFailureExceptions().add(t.toString());
					}
				}
				batchStep.setCommitCount(step.getCommitCount());
				batchStep.setEndTime(step.getEndTime());
				batchStep.setFilterCount(step.getFilterCount());
				batchStep.setLastUpdated(step.getLastUpdated());
				batchStep.setProcessSkipCount(step.getProcessSkipCount());
				batchStep.setReadCount(step.getReadCount());
				batchStep.setReadSkipCount(step.getReadSkipCount());
				batchStep.setRollbackCount(step.getRollbackCount());
				batchStep.setStartTime(step.getStartTime());
				batchStep.setStepName(step.getStepName());
				batchStep.setTerminateOnly(step.isTerminateOnly());
				batchStep.setWriteCount(step.getWriteCount());
				batchStep.setWriteSkipCount(step.getWriteSkipCount());
				if (step.getStatus() != null) {
					batchStep.setStatus(step.getStatus().toString());
				}
				if (step.getExitStatus() != null) {
					batchStep.setExitStatus(step.getExitStatus().getExitCode());
					batchStep.setExitDescription(step.getExitStatus().getExitDescription());
				}

				execution.getSteps().add(batchStep);
			}
		}

		execution.setGuid((String) jobExecution.getExecutionContext().get("guid"));
		execution.setRunning(jobExecution.isRunning());
		execution.setStopping(jobExecution.isStopping());
		return execution;
	}
}
