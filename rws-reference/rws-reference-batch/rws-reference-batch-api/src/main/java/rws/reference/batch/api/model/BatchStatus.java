package rws.reference.batch.api.model;

import java.util.List;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "running", "history" })
public class BatchStatus {

	@JsonProperty
	private List<BatchJob> running;

	@JsonProperty
	private List<BatchJob> history;

	/**
	 * @return the running
	 */
	public List<BatchJob> getRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public void setRunning(List<BatchJob> running) {
		this.running = running;
	}

	/**
	 * @return the history
	 */
	public List<BatchJob> getHistory() {
		return history;
	}

	/**
	 * @param history the history to set
	 */
	public void setHistory(List<BatchJob> history) {
		this.history = history;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}