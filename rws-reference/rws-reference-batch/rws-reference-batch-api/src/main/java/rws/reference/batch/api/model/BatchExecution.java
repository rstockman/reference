package rws.reference.batch.api.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.StandardToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import rws.reference.batch.api.utils.CustomJsonTimestampSerializer;

@JsonSerialize(include = Inclusion.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "id", "guid", "name", "jobStatus", "started", "ended", "durationMillis", "exitStatus", "running", "stopping",
		"parameters", "steps" })
public class BatchExecution {

	@JsonProperty
	private String guid;

	@JsonProperty
	private Long id;

	@JsonProperty
	private String name;

	@JsonProperty
	private String jobStatus;

	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	private Date started;

	@JsonProperty
	@JsonSerialize(using = CustomJsonTimestampSerializer.class, include = Inclusion.NON_NULL)
	private Date ended;

	@JsonProperty
	private Long durationMillis;

	@JsonProperty
	private String exitStatus;

	@JsonProperty
	private List<BatchStep> steps;

	@JsonProperty
	private Boolean running;

	@JsonProperty
	private Boolean stopping;

	@JsonProperty
	private List<BatchJobParameter> parameters;

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return jobStatus;
	}

	/**
	 * @param status the status to set
	 */
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * @return the started
	 */
	public Date getStarted() {
		return started;
	}

	/**
	 * @param started the started to set
	 */
	public void setStarted(Date started) {
		this.started = started;
	}

	/**
	 * @return the ended
	 */
	public Date getEnded() {
		return ended;
	}

	/**
	 * @param ended the ended to set
	 */
	public void setEnded(Date ended) {
		this.ended = ended;
	}

	/**
	 * @return the exitStatus
	 */
	public String getExitStatus() {
		return exitStatus;
	}

	/**
	 * @param exitStatus the exitStatus to set
	 */
	public void setExitStatus(String exitStatus) {
		this.exitStatus = exitStatus;
	}

	/**
	 * @return the steps
	 */
	public List<BatchStep> getSteps() {
		return steps;
	}

	/**
	 * @param steps the steps to set
	 */
	public void setSteps(List<BatchStep> steps) {
		this.steps = steps;
	}

	/**
	 * @return the running
	 */
	public Boolean getRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public void setRunning(Boolean running) {
		this.running = running;
	}

	/**
	 * @return the stopping
	 */
	public Boolean getStopping() {
		return stopping;
	}

	/**
	 * @param stopping the stopping to set
	 */
	public void setStopping(Boolean stopping) {
		this.stopping = stopping;
	}

	/**
	 * @return the durationMillis
	 */
	public Long getDurationMillis() {
		return durationMillis;
	}

	/**
	 * @param durationMillis the durationMillis to set
	 */
	public void setDurationMillis(Long durationMillis) {
		this.durationMillis = durationMillis;
	}

	/**
	 * @return the parameters
	 */
	public List<BatchJobParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<BatchJobParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setFieldSeparator(", ");
		style.setUseIdentityHashCode(false);
		return org.apache.commons.lang.builder.ToStringBuilder.reflectionToString(this, style);
	}

}